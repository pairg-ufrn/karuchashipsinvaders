--Game Global Variables
-- Contains all the constants used, like positions, colors RGB values and file paths.
-- Cointains all global variables, like game graphical layers and resource definitions.
-- Contains the only 2 global functions, they set game elements window position.

-- World and screen measures
WorldX  = 1024
WorldY  = 768
ScreenX = WorldX
ScreenY = WorldY
TOP     =  384
BOT     = -384
RIGHT   =  512
LEFT    = -512

-- Resource variables
barWidth = 256

kanjiFont1 = nil
kanjiFont2 = nil
numbersFont1 = nil
numbersFont2 = nil
romanjiFont1 = nil
romanjiFont2 = nil
romanjiFont3 = nil
romanjiFont4 = nil
textFont1 = nil
textFont2 = nil

-- Layers
backgroundLayer = nil
midLayer        = nil
hudLayer        = nil

-- Colors
colorTable = {["red"] = {0.75, 0, 0}, ["blue"] = {0, 0, 0.75}, ["green"] = {0, 0.75, 0}, ["black"] = {0, 0, 0}, ["grey"] = {0.28235, 0.28627, 0.28627}}

paused   = false
callGameHelp = false
callKanaHelp = false
callBossHelp = false
notHitG  = false
action   = true
changeCounter = false
callMainMenu  = false
loopTrue = true
--dateHour = os.date()

iHitsKana = 0
kanaFound = {}

iHitsBoss = 0
bossFound = {}

iKanaMissed = 0
kanaMissed = {}
answerKana = {}

iBossMissed = 0
bossMissed = {}
answerBoss = {}

helpLevel = {}

sessId = 0
newUser = 0
matchId = 0
stageId = 0
stageStatus = 0
serverVersion = 0
matchStatus = 0

stageModes = {easy = 1, medium = 2, hard = 3}

helpLevel[1] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29}

helpLevel[2] = {3, 6, 9, 12, 15, 18, 21, 24, 27, 30} 

kanaLevel = {}

kanaLevel[1]  = {"あ", "い", "う", "え", "お"}

kanaLevel[2]  = {"か", "き", "く", "け", "こ"}

kanaLevel[3]  = {"さ", "し", "す", "せ", "そ"}

kanaLevel[4]  = {"た", "ち", "つ", "て", "と"}

kanaLevel[5]  = {"な", "に", "ぬ", "ね", "の"}

kanaLevel[6]  = {"は", "ひ", "ふ", "へ", "ほ"}

kanaLevel[7]  = {"ま", "み", "む", "め", "も"}

kanaLevel[8]  = {"や", "ゆ", "よ"}

kanaLevel[9]  = {"ら", "り", "る", "れ", "ろ"}

kanaLevel[10] = {"わ", "を"}

kanaLevel[11] = {"ん"} 

kanaLevel[12] = {"が", "ぎ", "ぐ", "げ", "ご"}

kanaLevel[13] = {"ざ", "じ", "ず", "ぜ", "ぞ"}

kanaLevel[14] = {"だ", "ぢ", "づ", "で", "ど"}

kanaLevel[15] = {"ば", "び", "ぶ", "べ", "ぼ"}

kanaLevel[16] = {"ぱ", "ぴ", "ぷ", "ぺ", "ぽ"}

kanaLevel[17] = {"きゃ", "きゅ", "きょ"}

kanaLevel[18] = {"ぎゃ", "ぎゅ", "ぎょ"}

kanaLevel[19] = {"しゃ", "しゅ", "しょ"}

kanaLevel[20] = {"じゃ", "じゅ", "じょ"}

kanaLevel[21] = {"ちゃ", "ちゅ", "ちょ"}

kanaLevel[22] = {"ぢゃ", "ぢゅ", "ぢょ"}

kanaLevel[23] = {"にゃ", "にゅ", "にょ"}

kanaLevel[24] = {"ひゃ", "ひゅ", "ひょ"}

kanaLevel[25] = {"びゃ", "びゅ", "びょ"}

kanaLevel[26] = {"ぴゃ", "ぴゅ", "ぴょ"}

kanaLevel[27] = {"みゃ", "みゅ", "みょ"}

kanaLevel[28] = {"りゃ", "りゅ", "りょ"}

bossLevel = {}

bossLevel[1]  = {"すし", "さけ"}

bossLevel[2]  = {"したけ", "うちかけ", "はし", "とくさつ"}

bossLevel[3]  = {"きもの", "ゆかた", "はかま", "さしみ", "てまき", "のり", "あにめ", "からおけ", "ようかい", "かまくら"}

bossLevel[4]  = {"はおり", "げた", "にぎり", "ごはん", "すきやき", "まんが", "うんけい", "えんくう", "ががく"}

bossLevel[5]  = {"とめそで", "ふりそで", "いろそで", "てりやき", "しめじ", "おりがみ", "へいあん", "すどく", "やくざ"}

bossLevel[6]  = {"おび", "たび", "じかたび", "わさび", "やきそば", "てんぷら", "いけばな", "かぶき", "ぶがく", "さむらい"}

bossLevel[7]  = {"あにめ", "からおけ", "ようかい", "かまくら", "はおり", "げた", "にぎり"}

bossLevel[8]  = {"ごはん", "すきやき", "まんが", "うんけい", "えんくう", "ががく"}

bossLevel[9]  = {"とめそで", "ふりそで", "いろそで", "てりやき", "しめじ", "おりがみ"}

bossLevel[10] = {"へいあん", "すし", "さけ", "したけ", "うちかけ", "はし"}

pointerX = 0
pointerY = 0

click = 0

-- Context Help

currentLevel = 1

-- End of context help

bosses = {"sushi", "shitake", "sake", "uchikake", "hashi", "tokusatsu", "kimono", "yukata", "hakama", "sashimi", "temaki", "nori", "anime", "karaoke", "youkai", "kamakura", "haori", "geta", "nigiri", "gohan", "sukiyaki", "manga", "unkei", "enkuu", "gagaku", "tomesode", "furisode", "irosode", "teriyaki", "shimeji", "origami", "heian", "sudoku", "yakuza", "obi", "tabi", "jikatabi", "wasabi", "yakisoba", "tenpura", "ikebana", "kabuki", "bugaku", "samurai"--[[, "iromuji", "juunihitoe", "shoyu", "shari", "ukiyoe", "joumon", "geisha", "zori", "ohashori", "gyudon", "shabushabu", "shodou", "kyougen", "warashi", "kyahan", "wagashi", "sakuramochi", "shintou", "onnagata", "happi", "hippari", "korokke", "amanattou", "ningyoujoururi", "kumadori"]]}

achieved = {karucha_newbie=0, hiragana_newbie=0, karucha_beginner=0, karucha_reckless=0, karucha_cautious=0, karucha_on_the_go=0, hiragana_on_the_go=0, hiragana_warrior=0, hiragana_lover=0, karucha_master=0, karucha_sensei=0, goujon_master=0, handakuten_sensei=0}

gameSave = {["easy"] = 1, ["medium"] = 1, ["hard"] = 1, ["selected"] = "medium", ["sound"] = 1}

soundStopped = false

hitpath = ''
if seehitBox
  then hitpath = './images/seehitBox.png'
  else hitpath = './images/hitBox.png'
end 
------------------------------------------------
-- Resources for Kana Invaders game
------------------------------------------------
resource_definitions = {

  hud = {
    type = "image", 
    fileName = './images/level/hud.png', 
    width = 1024, height = 1024,
    position = {0, -1}
  },
 
  manha = {
    type = "image", 
    fileName = './images/level/manha.png', 
    width = 1024, height = 768,
    position = {0, 0}
  },

  tarde = {
    type = "image", 
    fileName = './images/level/tarde.png', 
    width = 1024, height = 768,
    position = {0, 0}
  },

  noite = {
    type = "image", 
    fileName = './images/level/noite.png', 
    width = 1024, height = 768,
    position = {0, 0}
  },

  barra = {
    type = "image", 
    fileName = './images/level/barra.png', 
    width = 256, height = 32,
    position = {230, 11}
  },

  flawless = {
    type = "image",
    fileName = './images/level/flawless.png',
    width = 586, height = 568
  },

  won = {
    type = "image",
    fileName = './images/level/won.png',
    width = 586, height = 568
  },

  gameover = {
    type = "image",
    fileName = './images/level/gameover.png',
    width = 586, height = 568
  },

  leftBox = {
    type = "image", 
    fileName = './images/level/box/leftBox.png',
    width = 222, height = 64,
    position = {178, 431}
  },

  centerBox = {
    type = "image", 
    fileName = './images/level/box/centerBox.png',
    width = 222, height = 64,
    position = {400, 431}
  },

  rightBox = {
    type = "image", 
    fileName = './images/level/box/rightBox.png',
    width = 222, height = 64,
    position = {622, 431}
  },

  topLeftBox = {
    type = "image", 
    fileName = './images/level/box/topLeftBox.png',
    width = 222, height = 64,
    position = {178, 132}
  },

  topRightBox = {
    type = "image", 
    fileName = './images/level/box/topRightBox.png',
    width = 222, height = 64,
    position = {622, 132}
  },

  button = {
    type = "image",
    fileName = './images/level/button.png',
    width = 224, height = 66
  },

  raio = {
    type = "image", 
    fileName = './images/effects/raio.png',
    width = 256, height = 1024
  },

  capturaNave = {
    type = "image", 
    fileName = './images/effects/capturaNave.png',
    width = 256, height = 256
  },

  cannon = {
    type = "image", 
    fileName = './images/elements/cannon.png', 
    width = 64, height = 64,
    position = {465, 649}
  },

  ship = {
    type = "image", 
    fileName = './images/elements/nave.png', 
    width = 256, height = 256
  },

  paraquedas = {
    type = "image", 
    fileName = './images/elements/paraquedas.png', 
    width = 512, height = 512
  },

  bonus = {
    type = "image", 
    fileName = './images/effects/bonus.png', 
    width = 64, height = 64,
    position = {363, 23}
  },

  romanjiFont1 = {
    type = "font",
    fileName = './fonts/menu&barra_arial.ttf',
    glyphs = " @abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789,.?!:<>()&|/-",
    fontSize = 9,
    dpi = 160
  },

  romanjiFont2 = {
    type = "font",
    fileName = './fonts/menu&barra_arial.ttf',
    glyphs = " @abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789,.?!:<>()&|/-",
    fontSize = 11.5,
    dpi = 160
  },

  romanjiFont3 = {
    type = "font",
    fileName = './fonts/menu&barra_arial.ttf',
    glyphs = " @abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789,.?!:<>()&|/-",
    fontSize = 19.1,
    dpi = 160
  },

  romanjiFont4 = {
    type = "font",
    fileName = './fonts/menu&barra_arial.ttf',
    glyphs = " @abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789,.?!:<>()&|/-",
    fontSize = 26.5,
    dpi = 160
  },

  kanjiFont1 = {
    type = "font",
    fileName = './fonts/jap.ttc',
    glyphs = "あいうえおかきくけこがぎぐげごさしすせそざじずぜぞたちつてとだぢづでどなにぬねのはひふへほばびぶべぼぱぴぷぺぽまみむめもやゆよらりるれろわをんっゃゅょ",
    fontSize = 10,
    dpi = 160
  },
  
  kanjiFont2 = {
    type = "font",
    fileName = './fonts/jap.ttc',
    glyphs = "あいうえおかきくけこがぎぐげごさしすせそざじずぜぞたちつてとだぢづでどなにぬねのはひふへほばびぶべぼぱぴぷぺぽまみむめもやゆよらりるれろわをんっゃゅょ",
    fontSize = 16,
    dpi = 160
  },

  numbersFont1 = {
    type = "font",
    fileName = './fonts/numeros_mool.ttf',
    glyphs = " @abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789,.?!:<>()&|/-",
    fontSize = 17,
    dpi = 160
  },

  numbersFont2 = {
    type = "font",
    fileName = './fonts/numeros_mool.ttf',
    glyphs = " @abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789,.?!:<>()&|/-",
    fontSize = 24,
    dpi = 160
  },

  textFont1 = {
    type = "font",
    fileName = './fonts/texto_janda.ttf',
    glyphs = " @abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789,.?!:<>()&|/-",
    fontSize = 14,
    dpi = 160
  },

  textFont2 = {
    type = "font",
    fileName = './fonts/texto_janda.ttf',
    glyphs = " @abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789,.?!:<>()&|/-",
    fontSize = 17,
    dpi = 160
  },

  levelTheme = {
    type = "sound",
    fileName = "./sounds/levelTheme.mp3",
    loop = true,
    volume = 0.7
  },

  shotSound = {
    type = "sound",
    fileName = "./sounds/shot.mp3",
    loop = false,
    volume = 1
  },

  explosionSound = {
    type = "sound",
    fileName = "./sounds/explosion.mp3",
    loop = false,
    volume = 1
  },

  bonusSound = {
    type = "sound",
    fileName = "./sounds/bonus.mp3",
    loop = false,
    volume = 1
  },

  -- Menu:
  
  menuBackground = {
    type = "image", 
    fileName = './images/menu/menuBackground.png', 
    width = 1024, height = 768
  },

  largePanel = {
    type = "image", 
    fileName = './images/menu/largePanel.png', 
    width = 1024, height = 768
  },

  smallPanel = {
    type = "image", 
    fileName = './images/menu/smallPanel.png', 
    width = 1024, height = 768
  },

  signinPanel = {
    type = "image", 
    fileName = './images/menu/signinPanel.png', 
    width = 334, height = 208
  },

  signupPanel = {
    type = "image", 
    fileName = './images/menu/signupPanel.png', 
    width = 334, height = 334
  },

  bar = {
    type = "image", 
    fileName = './images/menu/bar.png', 
    width = 1024, height = 768
  },

  about = {
    type = "image", 
    fileName = './images/menu/about.png',  
    width = 528, height = 100
  },

  ufrnBox = {
    type = "image", 
    fileName = './images/menu/box/ufrnBox.png',
    width = 150, height = 100,
    position = {248, 354}
  },

  dimapBox = {
    type = "image", 
    fileName = './images/menu/box/dimapBox.png',
    width = 125, height = 100,
    position = {398, 354}
  },

  pairgBox = {
    type = "image", 
    fileName = './images/menu/box/pairgBox.png',
    width = 253, height = 100,
    position = {523, 354}
  },

  karuchaBox = {
    type = "image", 
    fileName = './images/menu/box/karuchaBox.png',
    width = 543, height = 24,
    position = {240, 313}
  },

  space = {
    type = "image", 
    fileName = './images/menu/space.png',
    width = 252, height = 21
  },

  icone = {
    type = "image", 
    fileName = './images/menu/icone.png',  
    width = 165, height = 166.5
  },

  levels = {
    type = "image", 
    fileName = './images/menu/levels.png',  
    width = 1024, height = 768
  },

  levelShip = {
    type = "image", 
    fileName = './images/menu/levelShip.png',  
    width = 46, height = 28
  },

  selectedGreen = {
    type = "image", 
    fileName = './images/menu/selectedGreen.png',  
    width = 32, height = 32
  },

  selectedRed = {
    type = "image", 
    fileName = './images/menu/selectedRed.png',  
    width = 36, height = 36
  },

  unselected = {
    type = "image", 
    fileName = './images/menu/unselected.png',  
    width = 32, height = 32
  },

  hitBox = {
    type = "image", 
    fileName = hitpath, 
    width = 299, height = 48
  },

  language = {
    type = "image", 
    fileName = './images/menu/language.png', 
    width = 145, height = 48
  },

  backgroundMusic = {
    type = "sound",
    fileName = "./sounds/menuTheme.mp3",
    loop = true,
    volume = 0.76
  },
}

for i = 1, 6 do
  resource_definitions["1_" .. tostring(i)] = {type = "image", fileName = './images/story/1_' .. tostring(i) .. '.png', width = 1024, height = 1024}
end

for _ , boss in ipairs(bosses) do
  resource_definitions[boss] = {type = "image", fileName = './images/bosses/' .. boss .. '.png', width = 96, height = 96}
end

function setDefaultPos(prop, definitionName, optionalZ)
  local xwin, ywin = unpack(resource_definitions[definitionName].position)
  local halfWidth  = resource_definitions[definitionName].width/2
  local halfHeight = resource_definitions[definitionName].height/2
  xwin = xwin + halfWidth
  ywin = ywin + halfHeight
  local x, y = hudLayer:wndToWorld(xwin, ywin)
  if optionalZ == nil then
    prop:setLoc(x, y)
  else
    prop:setLoc(x, y, z)
  end
  prop.defaultX = x
  prop.defaultY = y
end

function setWinPos(prop, xwin, ywin)
  local x, y = hudLayer:wndToWorld(xwin, ywin)
  prop:setLoc(x, y)
end
-----------------
-- Karucha Server
-----------------
require("socket.http")

local file = io.open("server-url.txt", "r")

serverUrl = file:read() -- Capture file in a string

file:close()

-- DO NOT EVEN LOOK TO THESE FUNCTIONS --
-- Utilizado para gerar os campos das requisições
function countElementsInTable(table)
  local count = 0
  for _ in pairs(table) do count = count + 1 end
  return count
end

-- Gera uma string de campos http tomando por base uma tabela
function generateRequestFields(valueTable)
  local finalString = ""
  local numberOfFields = countElementsInTable(valueTable)
  local iteration = 0

  for index, value in pairs(valueTable) do
    finalString = finalString .. index .. "=" .. value
    iteration = iteration + 1
    if iteration < numberOfFields then
      finalString = finalString .. "&"
    end
  end

  return finalString
end

-- HTTP Get Request
function httpGETRequest(httpUrl, httpFields)
  local realUrl = nil
  if httpFields ~= nil then
    realUrl = httpUrl .. '?' .. generateRequestFields(httpFields)
  else
    realUrl = httpUrl
  end

  local response = socket.http.request(realUrl)
  return response
end

-- HTTP Post Request
function httpPOSTRequest(httpUrl, httpFields)
  local request_body = nil
  local content_length = 0
  if httpFields ~= nil then
    request_body = generateRequestFields(httpFields)
    content_length = #request_body
  end

  local response_body = {}
  

  socket.http.request
  {
    url = httpUrl;
    method = 'POST';
    headers =
    {
      ["Content-Type"] = "application/x-www-form-urlencoded";
      ["Content-Length"] = content_length;
    };
    source = ltn12.source.string(request_body);
    sink = ltn12.sink.table(response_body);
  }
  return table.concat(response_body)
end

-- Server functions - Use these when you want to comunicate with the server

-- Add User (working) --
function karuchaServerAddUser(f_name, f_nickname, f_email, f_username, f_password, f_permission)

  local karuchaHttpFields = {name = f_name, nickname = f_nickname, email = f_email, username = f_username, password = f_password, permission = f_permission, method = "add_user"}

  local response = httpPOSTRequest(serverUrl, karuchaHttpFields)
  local karuchaJsonResponse = MOAIJsonParser.decode(response)

  return karuchaJsonResponse["status"]
end

-- Login (working) --
function karuchaServerLogin(f_username, f_password)
  local karuchaHttpFields = {method = "login", username = f_username, password = f_password}

  local response = httpPOSTRequest(serverUrl, karuchaHttpFields)
  local karuchaJsonResponse = MOAIJsonParser.decode(response)

  if karuchaJsonResponse["status"] == 1 then
    return karuchaJsonResponse['contents']['sess_id']
  else
    return nil
  end
end

-- Logout (working) --
function karuchaServerLogout(f_sess_id)
  local karuchaHttpFields = {method = "logout", sess_id = f_sess_id}
  local response = httpPOSTRequest(serverUrl, karuchaHttpFields)
  local karuchaJsonResponse = MOAIJsonParser.decode(response)

  return karuchaJsonResponse["status"]
end

-- Open Match (working) --
function karuchaServerOpenMatch(f_sess_id, f_previous_match)
  
  local fields = {sess_id = f_sess_id, method = "open_match"}
  if f_previous_match ~= nil then
    fields["previous_match"] = f_previous_match
  end

  local response = httpPOSTRequest(serverUrl, fields)
  local karuchaJsonResponse = MOAIJsonParser.decode(response)

  if karuchaJsonResponse["status"] == 1 then
    return karuchaJsonResponse["contents"]["match_id"]
  else
    return 0
  end

end

-- Close Match (working) --
function karuchaServerCloseMatch(f_match_id, f_sess_id)
  local fields = {sess_id = f_sess_id, method = "close_match", match_id = f_match_id}

  local response = httpPOSTRequest(serverUrl, fields)
  local karuchaJsonResponse = MOAIJsonParser.decode(response)

  return karuchaJsonResponse["status"] 
end

-- Open Stage (working) --
function karuchaServerOpenStage(f_match_id, f_level_number, f_level_mode, f_sess_id)
  local karuchaHttpFields = {method = "open_stage", match_id = f_match_id, level_number = f_level_number, level_mode = f_level_mode, sess_id = f_sess_id}

  local response = httpPOSTRequest(serverUrl, karuchaHttpFields)
  local karuchaJsonResponse = MOAIJsonParser.decode(response)

  if karuchaJsonResponse["status"] == 1 then
    return karuchaJsonResponse['contents']['stage_id']
  else
    return nil
  end
end

-- Close Stage (working) --
function karuchaServerCloseStage(f_stage_id, f_play_time, f_status, f_sess_id)
  local karuchaHttpFields = {method = "close_stage", stage_id = f_stage_id, play_time = f_play_time, status = f_status, sess_id = f_sess_id}

  local response = httpPOSTRequest(serverUrl, karuchaHttpFields)
  local karuchaJsonResponse = MOAIJsonParser.decode(response)
  
  return karuchaJsonResponse["status"]
end

-- End Stage (working) --
function karuchaServerEndStage(f_stage_json, f_sess_id)
  local karuchaHttpFields = {method = "end_stage", stage_json = f_stage_json, sess_id = f_sess_id}

  local response = httpPOSTRequest(serverUrl, karuchaHttpFields)
  local karuchaJsonResponse = MOAIJsonParser.decode(response)

  return karuchaJsonResponse["status"]
end

-- Add Glyph (working) --
function karuchaServerAddGlyph(f_stage_id, f_glyph_type, f_glyph_column, f_glyph_row, f_glyph_status, f_sess_id)
  local karuchaHttpFields = {
    method = "add_glyph",
    stage_id = f_stage_id,
    glyph_type = f_glyph_type,
    glyph_column = f_glyph_column,
    glyph_row = f_glyph_row,
    status = f_glyph_status,
    sess_id = f_sess_id
  }

  local response = httpPOSTRequest(serverUrl, karuchaHttpFields)
  local karuchaJsonResponse = MOAIJsonParser.decode(response)

  return karuchaJsonResponse["status"]
end

-- Server Version (working) --
function karuchaServerVersion()
  local karuchaHttpFields = {method = 'version'}

  local response = httpPOSTRequest(serverUrl, karuchaHttpFields)
  local karuchaJsonResponse = MOAIJsonParser.decode(response)

  if karuchaJsonResponse["status"] == 1 then
    return karuchaJsonResponse['contents']['server_version']
  else
    return nil
  end

end