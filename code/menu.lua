local Menu =  {} -- The Menu module

-- Imports
local ResourceManager     = require "./code/resource_manager"
local ResourceDefinitions = require "./code/resource_definitions"
local Mouse = require "./code/mouse"
local Saves = require "./code/saves"
local Aux   = require "./code/auxiliar"

-- Module variables
local onMenu = true
local about  = false
local language = false
local menuOptions = {}
local signin = false
local signup = false
local index = 1
local input = ""
local input = {"", "", "", "", ""}
local enable = false
local online = false

-- Local menuScreen
local processInputFunction = function() end
local onOption = "Main"

-- Do the initial setup
function Menu:start()
  self:initialize()
  Menu:initializeBackgroundExtra()
  Menu:initializeBackground()
  if Menu:internet() then if sessId ~= 0 then Menu:wait("Log Out") else enable = true Menu:loadSignin() online = true end end
  while onMenu do
    loopTrue = true
    callMainMenu = false
    self:processInput()
    self:checkSound()
    coroutine.yield()
  end
  self.menuSound:stop()
end

function Menu:checkSound()
  if gameSave["sound"] == 0 and soundStopped == false then
    self.menuSound:stop()
    soundStopped = true
  elseif gameSave["sound"] == 1 and soundStopped == true then
    self.menuSound:play()
    soundStopped = false
    print("play")
  end
end
-------------------------------------------------------
-- initialize() does all the initial setup for the game
-------------------------------------------------------
function Menu:initialize()
  onMenu = true  

  local savedTable, err = Saves.load("./saves/achievements.sav")
  if err == nil then
    achieved = savedTable
    print("Sucessfully loaded achievements!")
  end

  savedTable, err = Saves.load("./saves/game.sav")
  if err == nil then
    gameSave = savedTable
    print("Sucessfully loaded game save!")
  end  

  -- We load all our resources
  ResourceDefinitions:setDefinitions(resource_definitions)
  kanjiFont2   = ResourceManager:get("kanjiFont2")
  numbersFont1 = ResourceManager:get("numbersFont1")
  romanjiFont1 = ResourceManager:get("romanjiFont1")
  romanjiFont2 = ResourceManager:get("romanjiFont2")
  romanjiFont3 = ResourceManager:get("romanjiFont3")
  romanjiFont4 = ResourceManager:get("romanjiFont4")
  textFont2    = ResourceManager:get("textFont2")

  MOAIRenderMgr.setRenderTable(nil)

  -- Layer initialization
  backgroundLayer = MOAILayer2D.new()
  backgroundLayer:setViewport(viewport)

  midLayer = MOAILayer2D.new()
  midLayer:setViewport(viewport)

  hudLayer = MOAILayer2D.new()
  hudLayer:setViewport(viewport)

  storyLayer = MOAILayer2D.new()
  storyLayer:setViewport(viewport)

  self:loadBackground()

  MOAIRenderMgr.setRenderTable({backgroundLayer, midLayer, hudLayer, storyLayer})

  Mouse:initialize()

  self.menuSound = ResourceManager:get("backgroundMusic")
  
  local box = {"ufrnBox", "dimapBox", "pairgBox", "karuchaBox"}

  self.buttonBox = {}

  for i = 1, 4 do
    self.buttonBox[i] = MOAIProp2D.new()
    self.buttonBox[i]:setDeck(ResourceManager:get(box[i]))
    setDefaultPos(self.buttonBox[i], box[i])
    self.buttonBox[i]:setBlendMode(MOAIProp.GL_SRC_ALPHA, MOAIProp.GL_ONE_MINUS_SRC_ALPHA)
    self.buttonBox[i]:setColor(1, 1, 1, 0)
  end

  if gameSave["sound"] == 1 then
    self.menuSound:play()
  end

  processInputFunction = mainMenuInput

  self.menuText = {}

  for i = 1, 6 do
    self.menuText[i] = MOAITextBox.new()
    self.menuText[i]:setFont(romanjiFont2)
    if i == 5 or i == 6 then self.menuText[i]:setRect(0, 0, 224, 448) else self.menuText[i]:setRect(0, 0, 560, 66) end

    self.menuText[i]:setColor(132, 134, 135)
    self.menuText[i]:setYFlip(true)
  end

  self.titleText1 = MOAITextBox.new()
  self.titleText1:setFont(textFont2)
  self.titleText1:setYFlip(true)
  self.titleText1:setRect(0, 0, 448, 132)
  self.titleText1:setAlignment(MOAITextBox.LEFT_JUSTIFY)

  self.titleText2 = MOAITextBox.new()
  self.titleText2:setFont(textFont2)
  self.titleText2:setYFlip(true)
  self.titleText2:setRect(0, 0, 448, 132)
  self.titleText2:setAlignment(MOAITextBox.LEFT_JUSTIFY)
end

function Menu:newOption(name, y, x)
  if x == nil then x = 515 end
  menuOptions[name] = MOAIProp2D.new()
  menuOptions[name]:setDeck(ResourceManager:get("hitBox"))
  setWinPos(menuOptions[name], x, y)
  hudLayer:insertProp(menuOptions[name])
end
-- Image in backgroundLayer ----------------------------------------
function Menu:loadBackground()
  self.backgroundProp = MOAIProp2D.new()
  self.backgroundProp:setDeck(ResourceManager:get("menuBackground"))
  self.backgroundProp:setLoc(0, 1)
  backgroundLayer:insertProp(self.backgroundProp)
  Menu:loadMainMenuOptions()
end
-- Image in midLayer -----------------------------------------------
function Menu:loadBar()
  self.barProp = MOAIProp2D.new()
  self.barProp:setDeck(ResourceManager:get("bar"))
  midLayer:insertProp(self.barProp)
end
-- Image in midLayer -----------------------------------------------
function Menu:loadAbout()
  self.aboutProp = MOAIProp2D.new()
  self.aboutProp:setDeck(ResourceManager:get("about"))
  self.aboutProp:setLoc(0, -20)
  midLayer:insertProp(self.aboutProp)
end
-- Image in midLayer -----------------------------------------------
function Menu:loadIcone()
  self.iconeProp = MOAIProp2D.new()
  self.iconeProp:setDeck(ResourceManager:get("icone"))
  self.iconeProp:setLoc(-167, 180)
  midLayer:insertProp(self.iconeProp)
end
-- Image in midLayer -----------------------------------------------
function Menu:loadLanguage()
  self.languageProp = MOAIProp2D.new()
  self.languageProp:setDeck(ResourceManager:get("language"))
  self.languageProp:setLoc(-50, 160)
  midLayer:insertProp(self.languageProp)
end
-- Image in midLayer -----------------------------------------------
function Menu:loadSigninPanel()
  self.signinPanelProp = MOAIProp2D.new()
  self.signinPanelProp:setDeck(ResourceManager:get("signinPanel"))
  self.signinPanelProp:setLoc(331, 43)
  midLayer:insertProp(self.signinPanelProp)
end
-- Image in midLayer -----------------------------------------------
function Menu:loadSignupPanel()
  self.signupPanelProp = MOAIProp2D.new()
  self.signupPanelProp:setDeck(ResourceManager:get("signupPanel"))
  self.signupPanelProp:setLoc(331, 43)
  midLayer:insertProp(self.signupPanelProp)
end

function Menu:loadSignin()
  Menu:loadSigninPanel() Menu:loadSign(2) Menu:loadInput(2)

  local text = {"Username:", "Password:"}
  
  for i = 1, 2 do
    midLayer:insertProp(self.buttonImg[i]) if enable then self.buttonImg[i]:setColor(1, 1, 1) enable = false else self.buttonImg[i]:setColor(25, 24, 23) end self.buttonImg[i]:setLoc(331, 116 - 42*i)

    midLayer:insertProp(self.signText[i]) self.signText[i]:setColor(132, 134, 135) self.signText[i]:setLoc(204, 127 - 42*i) self.signText[i]:setString(text[i])
  end
  midLayer:insertProp(self.buttonImg[6]) self.buttonImg[6]:setLoc(249, -19) self.buttonImg[6]:setScl(0.75, 0.75)
  midLayer:insertProp(self.buttonImg[7]) self.buttonImg[7]:setLoc(413, -19) self.buttonImg[7]:setScl(0.75, 0.75)

  midLayer:insertProp(self.buttonText1) self.buttonText1:setLoc(137, -55) self.buttonText1:setString("Sign Up")
  midLayer:insertProp(self.buttonText2) self.buttonText2:setLoc(301, -55) self.buttonText2:setString("Log In")

  signin = true signup = false
end

function Menu:loadSignup()
  Menu:loadSignupPanel() Menu:loadSign(5) Menu:loadInput(5)

  local text = {"Name:", "Nickname:", "e-mail:", "Username:", "Password:"}
  
  for i = 1, 5 do
    midLayer:insertProp(self.buttonImg[i]) self.buttonImg[i]:setColor(25, 24, 23) self.buttonImg[i]:setLoc(331, 179 - 42*i)

    midLayer:insertProp(self.signText[i]) self.signText[i]:setColor(132, 134, 135) self.signText[i]:setLoc(204, 190 - 42*i) self.signText[i]:setString(text[i])
  end
  midLayer:insertProp(self.buttonImg[6]) self.buttonImg[6]:setLoc(249, -82) self.buttonImg[6]:setScl(0.75, 0.75)
  midLayer:insertProp(self.buttonImg[7]) self.buttonImg[7]:setLoc(413, -82) self.buttonImg[7]:setScl(0.75, 0.75)

  midLayer:insertProp(self.buttonText1) self.buttonText1:setLoc(137, -118) self.buttonText1:setString("Log In")
  midLayer:insertProp(self.buttonText2) self.buttonText2:setLoc(301, -118) self.buttonText2:setString("Sign Up")

  signup = true signin = false 
end

function Menu:loadSign(limit)
  self.signText = {}

  for i = 1, limit do
    self.signText[i] = MOAITextBox.new()
    self.signText[i]:setFont(romanjiFont1)
    self.signText[i]:setYFlip(true)
    self.signText[i]:setRect(0, 0, 252, 23)
    self.signText[i]:setAlignment(MOAITextBox.LEFT_JUSTIFY)
  end
end

function Menu:loadInput(limit)
  self.inputText = {}

  for i = 1, limit do
    self.inputText[i] = MOAITextBox.new()
    self.inputText[i]:setFont(romanjiFont1)
    self.inputText[i]:setYFlip(true)
    self.inputText[i]:setRect(0, 0, 252, 23)
    self.inputText[i]:setAlignment(MOAITextBox.LEFT_JUSTIFY)
  end

  self.buttonImg = {}

  for i = 1, 7 do
    self.buttonImg[i] = MOAIProp2D.new()
    
    if i < 6 then self.buttonImg[i]:setDeck(ResourceManager:get("space")) else self.buttonImg[i]:setDeck(ResourceManager:get("button")) end
  
  end

  self.buttonText1 = MOAITextBox.new()
  self.buttonText1:setFont(numbersFont1)
  self.buttonText1:setRect(0, 0, 224, 66)
  Aux.centralize(self.buttonText1)

  self.buttonText1:setColor(132, 134, 135)
  self.buttonText1:setYFlip(true)

  self.buttonText2 = MOAITextBox.new()
  self.buttonText2:setFont(numbersFont1)
  self.buttonText2:setRect(0, 0, 224, 66)
  Aux.centralize(self.buttonText2)

  self.buttonText2:setColor(132, 134, 135)
  self.buttonText2:setYFlip(true)
end

function Menu:fields(mouseX, mouseY)

  if signin then
    if (mouseX > 204 and mouseX < 457) and (mouseY >   64 and mouseY <   86) then index = 1 end
    if (mouseX > 204 and mouseX < 457) and (mouseY >   22 and mouseY <   44) then index = 2 end
    if (mouseX > 165 and mouseX < 331) and (mouseY >  -44 and mouseY <    6) then index = 3 end
    if (mouseX > 331 and mouseX < 497) and (mouseY >  -44 and mouseY <    6) then index = 4 end
  end

  if signup then
    if (mouseX > 204 and mouseX < 457) and (mouseY >  127 and mouseY <  149) then index = 1 end
    if (mouseX > 204 and mouseX < 457) and (mouseY >   85 and mouseY <  107) then index = 2 end
    if (mouseX > 204 and mouseX < 457) and (mouseY >   43 and mouseY <   65) then index = 3 end
    if (mouseX > 204 and mouseX < 457) and (mouseY >    1 and mouseY <   23) then index = 4 end
    if (mouseX > 204 and mouseX < 457) and (mouseY >  -41 and mouseY <  -19) then index = 5 end
    if (mouseX > 165 and mouseX < 331) and (mouseY > -107 and mouseY <  -57) then index = 6 end
    if (mouseX > 331 and mouseX < 497) and (mouseY > -107 and mouseY <  -57) then index = 7 end
  end

  if not signin and not signup and online then
    if (mouseX > 334 and mouseX < 413) and (mouseY >  364 and mouseY <  384) then index = 1 Menu:wait("") Menu:loadSignin() local logoutStatus = karuchaServerLogout(sessId) print("\nUSER LOGOUT: " .. logoutStatus .. "\n") end
  end

  if signin and index == 3 then Menu:remove() Menu:loadSignup() end
  if signup and index == 6 then Menu:remove() Menu:loadSignin() end
  
  if signin and index == 4 and input[1] ~= "" and input[2] ~= "" then sessId = karuchaServerLogin(input[1], input[2]) if sessId ~= nil then print("\nSESSION ID: " .. sessId .. "\n") Menu:remove() Menu:wait("Log Out") else print("\nUSERNAME OR PASSWORD ENTERED IS INCORRECT.\n") end else Menu:selected() end
  
  if signup and index == 7 and input[1] ~= "" and input[2] ~= "" and input[3] ~= "" and input[4] ~= "" and input[5] ~= "" then newUser = karuchaServerAddUser(input[1], input[2], input[3], input[4], input[5], 1) if newUser ~= 0 then Menu:remove() enable = true Menu:loadSignin() print("\nNEW USER ADDED.\n") else print("\nNEW USER NOT ADDED. FILL IN THE FIELDS CORRECTLY.\n") end else Menu:selected() end

end

function Menu:selected()

  local limit = 0

  if signin then limit = 2 end
  if signup then limit = 5 end
  
  for i = 1, limit do
    
    if i == index then

      for j = 1, limit do
        midLayer:removeProp(Menu.buttonImg[j])

        if j == index then self.buttonImg[j]:setColor(1, 1, 1) else self.buttonImg[j]:setColor(25, 24, 23) end
        
        midLayer:insertProp(Menu.buttonImg[j])
      end
    end
  end
end

function Menu:inputBuffer(key)
  
  if (key > 47 and key < 58) or (key > 64 and key < 91) or (key > 96 and key < 123) or index == 3 and (key == 46 or key == 64) or index == 1 and key == 32 then

    input[index] = input[index] .. string.char(tostring(key))

  end

  if key == 8 then

    input[index] = string.sub(input[index], 0, #input[index] - 1)

  end

  midLayer:insertProp(self.inputText[index]) self.inputText[index]:setColor(132, 134, 135) self.inputText[index]:setString(input[index])

  if signin then self.inputText[index]:setLoc(205, 107 - 42*index) end
  if signup then self.inputText[index]:setLoc(205, 169 - 42*index) end

end

function Menu:remove()

  local limit = 0

  if signin then limit = 2 end
  if signup then limit = 5 end
  
  if signin and (index == 3 or index == 4) then midLayer:removeProp(Menu.signinPanelProp) signin = false end
  if signup and (index == 6 or index == 7) then midLayer:removeProp(Menu.signupPanelProp) signup = false end

  for i = 1, 7 do midLayer:removeProp(Menu.buttonImg[i]) if i <= limit then midLayer:removeProp(Menu.signText[i]) midLayer:removeProp(Menu.inputText[i]) end end

  midLayer:removeProp(Menu.buttonText1) midLayer:removeProp(Menu.buttonText2) index = 1 input = {"", "", "", "", ""}
end

function Menu:wait(text)
  midLayer:insertProp(self.signText[1]) self.signText[1]:setColor(198, 132, 124) self.signText[1]:setLoc(334, 365) self.signText[1]:setString(text)
end

function Menu:unloadMenuOptions()
  for _ , prop in pairs(menuOptions) do
    hudLayer:removeProp(prop)
  end
end

function Menu:unloadMenuScreen()
  midLayer:removeProp(self.screenProp)
  hudLayer:removeProp(menuOptions["Back"])
end

function Menu:loadMainMenuOptions()
  Menu:newOption("Start", 140)
  Menu:newOption("Top10", 215)
  Menu:newOption("Achievements"  , 264)
  Menu:newOption("Configurations", 313)
  Menu:newOption("Help" , 362)
  Menu:newOption("About", 411)
  Menu:newOption("Exit" , 460)
end

function Menu:loadMenuScreen(screenName)
  self.screenProp = MOAIProp2D.new() -- Um prop é criado
  self.screenProp:setDeck(ResourceManager:get(screenName)) -- Setar a imagem do prop como a definida na tabela de resource_definitions com o nome passado como parametro para essa função (no caso "help")
  self.screenProp:setLoc(0, 1)
  midLayer:insertProp(self.screenProp) -- Insira esse prop na camada "midLayer"
  -- A partir daqui não faz diferença para o seu uso dessa função, é tudo inutil para o segundo uso.
  -- Essas 4 linhas são tratamentos feitos para mudanças de uma tela de menu para outra, mas na transição
  -- Jogo -> Menu e Menu -> Jogo essas opções não fazem diferença
  Menu:unloadMenuOptions() -- Descarregue qualquer prop de menu (ignorando essa parte por enquanto)
  Menu:newOption("Back", 25, 400) -- A chamada a essa função vai adicionar um prop na tabela ("vetor") menuOptions.
                                  -- Essa tabela tem como indices strings (como "Back") que mapeia para um prop.
                                  -- Esse prop é adicionado em uma entrada menuOptions["Back"] = prop.
                                  -- Essa função (newOption) cria um Prop mas coloca como imagem (deck) dele uma imagem INVISIVEL
                                  -- (veja em images/hitBox que ela é uma imagem de um quadrado totalmente transparente).
                                  -- Essa imagem invisivel foi uma forma que eu fiz para facilitar detectar quando o mouse está 
                                  -- Em cima de uma opção, pois ver se o mouse está em cima de um prop é facil, e carregar uma imagem 
                                  -- Em um prop já redimensiona ele automaticamente para as dimensões de imagem definidas
                                  -- Na tabela resource_definitions. Na situação atual, a chamada a partir do jogo, esse prop 
                                  -- Não faz diferença nenhuma pois no código do jogo não existe tratamento do mouse para ver se ele está 
                                  -- Em cima do Back. Essa linha seria para criar um prop clicavel com o mouse,
                                  -- Mas como no código do jogo não existe tratamento de mouse esse prop-botão é completamente inutil.
  processInputFunction = screenMenuInput -- No menu eu trocava a função de tratamento do mouse dependendo da tela em que estivesse
  onOption = screenName -- Indica em qual tela do menu o jogador está
end

function Menu:prepareConfigScreen()
  soundOnProp = MOAIProp2D.new()
  soundOnProp:setDeck(ResourceManager:get("unselected"))
  soundOnProp:setLoc(-99, 222)
  hudLayer:insertProp(soundOnProp)

  soundOffProp = MOAIProp2D.new()
  soundOffProp:setDeck(ResourceManager:get("unselected"))
  soundOffProp:setLoc(6, 222)
  hudLayer:insertProp(soundOffProp)

  if gameSave["sound"] == 1 then
    soundOnProp:setDeck(ResourceManager:get("selectedRed"))
  elseif gameSave["sound"] == 0 then
    soundOffProp:setDeck(ResourceManager:get("selectedRed"))
  end 
end

function Menu:unprepareConfigScreen()
  if soundOffProp ~= nil then
    hudLayer:removeProp(soundOffProp)
  end
  if soundOnProp ~= nil then
    hudLayer:removeProp(soundOnProp)
  end
end

function Menu:unprepareLevelScreen()
  if easyProp ~= nil then
    hudLayer:removeProp(easyProp)
    hudLayer:removeProp(mediumProp)
    hudLayer:removeProp(hardProp)
    for _, prop in ipairs(levelsProp) do
      hudLayer:removeProp(prop)
    end
  end
end

function Menu:prepareLevelScreen()
  easyProp = MOAIProp2D.new()
  easyProp:setDeck(ResourceManager:get("unselected"))
  easyProp:setLoc(-167, 222)
  hudLayer:insertProp(easyProp)

  mediumProp = MOAIProp2D.new()
  mediumProp:setDeck(ResourceManager:get("unselected"))
  mediumProp:setLoc(0, 222)
  hudLayer:insertProp(mediumProp)

  hardProp = MOAIProp2D.new()
  hardProp:setDeck(ResourceManager:get("unselected"))
  hardProp:setLoc(125, 222)
  hudLayer:insertProp(hardProp)

  if gameSave["selected"] == "easy" then
    easyProp:setDeck(ResourceManager:get("selectedRed"))
  elseif gameSave["selected"] == "medium" then
    mediumProp:setDeck(ResourceManager:get("selectedRed"))
  else
    hardProp:setDeck(ResourceManager:get("selectedRed"))
  end

  levelsProp = {}
  local difficulty = gameSave["selected"]
  for i = 1, gameSave[difficulty] do
    levelsProp[i] = MOAIProp2D.new()
    levelsProp[i]:setDeck(ResourceManager:get("levelShip"))
    local x = 170 + math.ceil(i/3)*60
    local md = i % 3 if md == 0 then md = 3 end
    local y =  263 + md * 37
    local wx, wy = hudLayer:wndToWorld(x, y)
    if i > 15 then wx = wx + 20 end
    levelsProp[i]:setLoc(wx, wy)
    levelsProp[i]:setScl(1.1, 1.1)
    if i ~= 31 then -- Esse if impede que seja criada uma "nave fantasma" de level 31 quando o jogo é zerado
      hudLayer:insertProp(levelsProp[i])
    end
  end
end
-------------------------------------------------------
-- processInput() talks to InputManager to handle input
-------------------------------------------------------
function Menu:processInput()
    processInputFunction()
end

function mainMenuInput()
  if Mouse:click() then
    Menu:fields(Mouse:worldPosition())
    if Mouse:onProp(menuOptions["Start"]) then
      Menu:loadMenuScreen("levels")
      Menu:loadBar()
      Menu:loadTitle1("Difficulty mode", -282, 198) Menu:loadTitle2("Level", -282, 63) Menu:loadMenuText1("right","easy", -762, 174) Menu:loadMenuText2("right","medium", -595, 174) Menu:loadMenuText3("right","hard", -470, 174)
      Menu:prepareLevelScreen()
    elseif Mouse:onProp(menuOptions["Top10"]) then
      Menu:loadMenuScreen("largePanel")
      Menu:loadBar()
      Menu:loadTitle1("Top 10", -282, 198)
    elseif Mouse:onProp(menuOptions["Achievements"]) then
      Menu:loadMenuScreen("largePanel")
      Menu:loadBar()
      Menu:loadTitle1("Achievements", -282, 198)
      for name, value in pairs(achieved) do
        if value == 1 then print(name) end
      end
    elseif Mouse:onProp(menuOptions["Configurations"]) then
      Menu:loadMenuScreen("smallPanel")
      Menu:loadBar()
      Menu:loadTitle1("Configurations", -282, 198) Menu:loadMenuText1("right","sound", -743, 174) Menu:loadMenuText2("right","on", -694, 174) Menu:loadMenuText3("right","off", -588, 174) Menu:loadMenuText4("right","language", -705, 111)
      Menu:loadLanguage()
      Menu:prepareConfigScreen()
      language = true
    elseif Mouse:onProp(menuOptions["Help"]) then
      Menu:loadMenuScreen("largePanel")
      Menu:loadBar()
      Menu:loadTitle1("Help", -282, 198) Menu:loadMenuText5("left","(P)\n(T)\n(A)\n(C)\n(H)\n(B)\n(X)\n\n(ESC)", -260, -200) Menu:loadMenuText6("left","Play Game\nTop10\nAchievements\nConfigurations\nHelp\nAbout\nExit\n\nBack", -160, -200)
    elseif Mouse:onProp(menuOptions["About"]) then
      Menu:loadMenuScreen("largePanel")
      Menu:loadBar()
      Menu:loadIcone()
      Menu:loadAbout()
      Menu:loadTitle1("About", -282, 198) Menu:loadMenuText1("left","Version 0.4.1", 0, 115) Menu:loadMenuText2("left","www.karucha.pairg.dimap.ufrn.br/creditos/", -273, 12)
      for i = 1, 4 do hudLayer:insertProp(Menu.buttonBox[i]) end
      about = true
    elseif Mouse:onProp (menuOptions["Exit"]) then
      os.exit()
    end
    print(Mouse:position())
  end
  -- Menu shortcut keyboard keys
  MOAIInputMgr.device.keyboard:setCallback(
    function(key, down)
      if down == true then

        if (signin and index ~= 4) or (signup and index ~= 7) then Menu:inputBuffer(key) else index = 1 end
        
        if (key == 80 or key == 112) and not signin and not signup then
          Menu:loadMenuScreen("levels")
          Menu:loadBar()
          Menu:loadTitle1("Difficulty mode", -282, 198) Menu:loadTitle2("Level", -282, 63) Menu:loadMenuText1("right","easy", -762, 174) Menu:loadMenuText2("right","medium", -595, 174) Menu:loadMenuText3("right","hard", -470, 174)
          Menu:prepareLevelScreen()
        elseif (key == 67 or key == 99) and not signin and not signup then
          Menu:loadMenuScreen("smallPanel")
          Menu:loadBar()
          Menu:loadTitle1("Configurations", -282, 198) Menu:loadMenuText1("right","sound", -743, 174) Menu:loadMenuText2("right","on", -694, 174) Menu:loadMenuText3("right","off", -588, 174) Menu:loadMenuText4("right","language", -705, 111)
          Menu:loadLanguage()
          Menu:prepareConfigScreen()
          language = true
        elseif (key == 65 or key == 97) and not signin and not signup then
          Menu:loadMenuScreen("largePanel")
          Menu:loadBar()
          Menu:loadTitle1("Achievements", -282, 198)
          for name, value in pairs(achieved) do
            if value == 1 then print(name) end
          end
        elseif (key == 84 or key == 116) and not signin and not signup then
          Menu:loadMenuScreen("largePanel")
          Menu:loadBar()
          Menu:loadTitle1("Top 10", -282, 198)
        elseif (key == 66 or key == 98) and not signin and not signup then
          Menu:loadMenuScreen("largePanel")
          Menu:loadBar()
          Menu:loadIcone()
          Menu:loadAbout()
          Menu:loadTitle1("About", -282, 198) Menu:loadMenuText1("left","Version 0.4.1", 0, 115) Menu:loadMenuText2("left","www.karucha.pairg.dimap.ufrn.br/creditos/", -273, 12)
          for i = 1, 4 do hudLayer:insertProp(Menu.buttonBox[i]) end
          about = true
        elseif (key == 72 or key == 104) and not signin and not signup then
          Menu:loadMenuScreen("largePanel")
          Menu:loadBar()
          Menu:loadTitle1("Help", -282, 198) Menu:loadMenuText5("left","(P)\n(T)\n(A)\n(C)\n(H)\n(B)\n(X)\n\n(ESC)", -260, -200) Menu:loadMenuText6("left","Play Game\nTop10\nAchievements\nConfigurations\nHelp\nAbout\nExit\n\nBack", -160, -200)
        elseif (key == 88 or key == 120) and not signin and not signup then
          os.exit()
        end
      end
    end
  )  
end

function notInArea()
  local mouseX, mouseY = Mouse:worldPosition()
  yLimit = -110
  if onOption == "smallPanel" then yLimit = 87 end
  return mouseX < -333 or mouseX > 333 or mouseY < yLimit
end

function loadStory(n)
  storyScreen = MOAIProp2D.new()
  storyScreen:setDeck(ResourceManager:get(tostring(n) .. "_1"))
  storyScreen:setLoc(0, 0)
  storyLayer:insertProp(storyScreen)
  local part = 1
  while part < 7 do
    if Mouse:click() then
      part = part + 1
      if part < 7 then
        storyScreen:setDeck(ResourceManager:get(tostring(n) .. "_" .. tostring(part)))
      end
    end
    coroutine.yield()
  end
  onMenu = false
end

function screenMenuInput ()
  if Mouse:click() then
    if Mouse:onProp(menuOptions["Back"]) or notInArea() then
      hudLayer:removeProp(Menu.titleText1)
      hudLayer:removeProp(Menu.titleText2)
      
      for i = 1, 6 do hudLayer:removeProp(Menu.menuText[i]) end

      midLayer:removeProp(Menu.screenProp)
      midLayer:removeProp(Menu.barProp)
      if about then
        midLayer:removeProp(Menu.iconeProp)
        midLayer:removeProp(Menu.aboutProp)
        for i = 1, 4 do hudLayer:removeProp(Menu.buttonBox[i]) end
        about = false
      end
      if language then
        midLayer:removeProp(Menu.languageProp)
      end
      Menu:unloadMenuOptions()
      Menu:unprepareLevelScreen()
      Menu:unprepareConfigScreen()
      Menu:loadMainMenuOptions()
      onOption = "Main"
      processInputFunction = mainMenuInput
    elseif Mouse:onProp(Menu.buttonBox[1]) and about then os.execute("cmd.exe /c start http://www.ufrn.br")
    elseif Mouse:onProp(Menu.buttonBox[2]) and about then os.execute("cmd.exe /c start http://www.dimap.ufrn.br")
    elseif Mouse:onProp(Menu.buttonBox[3]) and about then os.execute("cmd.exe /c start http://www.pairg.dimap.ufrn.br")
    elseif Mouse:onProp(Menu.buttonBox[4]) and about then os.execute("cmd.exe /c start http://www.karucha.pairg.dimap.ufrn.br/creditos")
    end

    if (levelsProp ~= nil and onOption == "levels") then
      for i, prop in ipairs (levelsProp) do
        if Mouse:onProp (prop) then
          startLevelParameter = i
          if startLevelParameter == 1 then
            loadStory(1)
          else
            onMenu = false
          end
          break
        end
      end
    end

    if onOption == "smallPanel" then
      if Mouse:onProp(soundOffProp) then
        gameSave["sound"] = 0
        Menu:unprepareConfigScreen()
        Menu:prepareConfigScreen()
      elseif Mouse:onProp(soundOnProp) then
        gameSave["sound"] = 1
        Menu:unprepareConfigScreen()
        Menu:prepareConfigScreen()
      end
    else
      if Mouse:onProp(easyProp) then
        gameSave["selected"] = "easy"
        Menu:unprepareLevelScreen()
        Menu:prepareLevelScreen()
      elseif Mouse:onProp(mediumProp) then
        gameSave["selected"] = "medium"
        Menu:unprepareLevelScreen()
        Menu:prepareLevelScreen()
      elseif Mouse:onProp(hardProp) then
        gameSave["selected"] = "hard"
        Menu:unprepareLevelScreen()
        Menu:prepareLevelScreen()
      end
    end
    Saves.save(gameSave, "./saves/game.sav") -- Salva as alterações
    print(Mouse:worldPosition())
  end
  --Keyboard navigation
  MOAIInputMgr.device.keyboard:setCallback(
    function(key, down)
      if down == true then
        -- Configurations
        if onOption == "smallPanel" then
          if key == 46 then -- TO DO: Change for right arrow
            gameSave["sound"] = 0
            Menu:unprepareConfigScreen()
            Menu:prepareConfigScreen()
          elseif key == 44 then -- TO DO: Change for left arrow
            gameSave["sound"] = 1
            Menu:unprepareConfigScreen()
            Menu:prepareConfigScreen()
          end
        end
        -- Back with keyboard shortcut ESC
        if key == 27 then
          hudLayer:removeProp(Menu.titleText1)
          hudLayer:removeProp(Menu.titleText2)
          
          for i = 1, 6 do hudLayer:removeProp(Menu.menuText[i]) end
          
          midLayer:removeProp(Menu.screenProp)
          midLayer:removeProp(Menu.barProp)
          if about then
            midLayer:removeProp(Menu.iconeProp)
            midLayer:removeProp(Menu.aboutProp)
          end
          if language then
            midLayer:removeProp(Menu.languageProp)
          end
          Menu:unloadMenuOptions()
          Menu:unprepareLevelScreen()
          Menu:unprepareConfigScreen()
          Menu:loadMainMenuOptions()
          onOption = "Main"
          processInputFunction = mainMenuInput
        end
      end
    end
  )
end
-- Message in midLayer ----------------------------------------
function Menu:initializeBackgroundExtra()
  self.backgroundExtra = MOAITextBox.new()
  self.backgroundExtra:setFont(romanjiFont4)
  self.backgroundExtra:setYFlip(true)
  self.backgroundExtra:setRect(-290, -290, 290, 290)
  self.backgroundExtra:setColor(199, 133, 125)
  self.backgroundExtra:setString("Play Game")
  self.backgroundExtra:setAlignment(MOAITextBox.CENTER_JUSTIFY)
  midLayer:insertProp(self.backgroundExtra)
end
-- Message in midLayer ----------------------------------------
function Menu:initializeBackground()
  self.background = MOAITextBox.new()
  self.background:setFont(romanjiFont3)
  self.background:setYFlip(true)
  self.background:setRect(-198, -198, 198, 198)
  self.background:setColor(199, 133, 125)
  self.background:setString("Top 10\nAchievements\nConfigurations\nHelp\nAbout\nExit")
  self.background:setAlignment(MOAITextBox.CENTER_JUSTIFY)
  midLayer:insertProp(self.background)
end

function Menu:loadMenuText1(alignment, text, x, y)
  hudLayer:insertProp(self.menuText[1]) self.menuText[1]:setLoc(x, y) self.menuText[1]:setString(text) if alignment == "right" then self.menuText[1]:setAlignment(MOAITextBox.RIGHT_JUSTIFY) elseif alignment == "left" then self.menuText[1]:setAlignment(MOAITextBox.LEFT_JUSTIFY) end
end

function Menu:loadMenuText2(alignment, text, x, y)
  hudLayer:insertProp(self.menuText[2]) self.menuText[2]:setLoc(x, y) self.menuText[2]:setString(text) if alignment == "right" then self.menuText[2]:setAlignment(MOAITextBox.RIGHT_JUSTIFY) elseif alignment == "left" then self.menuText[2]:setAlignment(MOAITextBox.LEFT_JUSTIFY) end
end

function Menu:loadMenuText3(alignment, text, x, y)
  hudLayer:insertProp(self.menuText[3]) self.menuText[3]:setLoc(x, y) self.menuText[3]:setString(text) if alignment == "right" then self.menuText[3]:setAlignment(MOAITextBox.RIGHT_JUSTIFY) elseif alignment == "left" then self.menuText[3]:setAlignment(MOAITextBox.LEFT_JUSTIFY) end
end

function Menu:loadMenuText4(alignment, text, x, y)
  hudLayer:insertProp(self.menuText[4]) self.menuText[4]:setLoc(x, y) self.menuText[4]:setString(text) if alignment == "right" then self.menuText[4]:setAlignment(MOAITextBox.RIGHT_JUSTIFY) elseif alignment == "left" then self.menuText[4]:setAlignment(MOAITextBox.LEFT_JUSTIFY) end
end

function Menu:loadMenuText5(alignment, text, x, y)
  hudLayer:insertProp(self.menuText[5]) self.menuText[5]:setLoc(x, y) self.menuText[5]:setString(text) if alignment == "right" then self.menuText[5]:setAlignment(MOAITextBox.RIGHT_JUSTIFY) elseif alignment == "left" then self.menuText[5]:setAlignment(MOAITextBox.LEFT_JUSTIFY) end
end

function Menu:loadMenuText6(alignment, text, x, y)
  hudLayer:insertProp(self.menuText[6]) self.menuText[6]:setLoc(x, y) self.menuText[6]:setString(text) if alignment == "right" then self.menuText[6]:setAlignment(MOAITextBox.RIGHT_JUSTIFY) elseif alignment == "left" then self.menuText[6]:setAlignment(MOAITextBox.LEFT_JUSTIFY) end
end

function Menu:loadTitle1(text, x, y)
  hudLayer:insertProp(self.titleText1) self.titleText1:setColor(132, 134, 135) self.titleText1:setLoc(x, y) self.titleText1:setString(text)
end

function Menu:loadTitle2(text, x, y)
  hudLayer:insertProp(self.titleText2) self.titleText2:setColor(132, 134, 135) self.titleText2:setLoc(x, y) self.titleText2:setString(text)
end
-----------------------------------------------------------------------------
-- sleepCoroutine(time) helper method to freeze the thread for "time" seconds
-----------------------------------------------------------------------------
function sleepCoroutine(time)
  local timer = MOAITimer.new()
  timer:setSpan(time)
  timer:start()
  MOAICoroutine.blockOnAction(timer)
end

function sleep(time)
  local st = os.time()
  while os.time() - st < time do
    coroutine.yield()
  end
end

function Menu:internet()

  http = require("socket.http")

  local connection = http.request(serverUrl) if connection then print("\nKARUCHA SERVER URL: " .. serverUrl .. "\n\nKARUCHA SERVER - VERSION: " .. karuchaServerVersion() .. "\n") return true else return false end

end

return Menu