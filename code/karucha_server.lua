require 'socket.http'
json = require 'json'

KARUCHA_SERVER_URL = "http://dev.karucha.pairg.dimap.ufrn.br"

-- DO NOT EVEN LOOK TO THESE FUNCTIONS --
-- Utilizado para gerar os campos das requisições
function countElementsInTable(table)
  local count = 0
	for _ in pairs(table) do count = count + 1 end
	return count
end

-- Gera uma string de campos http tomando por base uma tabela
function generateRequestFields(valueTable)
	local	finalString = ""
	local numberOfFields = countElementsInTable(valueTable)
	local iteration = 0

	for index, value in pairs(valueTable) do
  	finalString = finalString .. index .. "=" .. value
		iteration = iteration + 1
		if iteration < numberOfFields then
			finalString = finalString .. "&"
		end
	end

	return finalString
end

-- HTTP Get Request
function httpGETRequest(httpUrl, httpFields)
	local realUrl = nil
	if httpFields ~= nil then
		realUrl = httpUrl .. '?' .. generateRequestFields(httpFields)
	else
		realUrl = httpUrl
	end

	local response = socket.http.request(realUrl)
	return response
end

-- HTTP Post Request
function httpPOSTRequest(httpUrl, httpFields)
	local request_body = nil
	local content_length = 0
	if httpFields ~= nil then
		request_body = generateRequestFields(httpFields)
		content_length = #request_body
	end

	local response_body = { }
	

	socket.http.request
	{
		url = httpUrl;
		method = 'POST';
		headers =
		{
			["Content-Type"] = "application/x-www-form-urlencoded";
			["Content-Length"] = content_length;
		};
		source = ltn12.source.string(request_body);
		sink = ltn12.sink.table(response_body);
	}
	return table.concat(response_body)
end

-- OKAY, NOW YOU CAN LOOK --
-- Server functions - Use these when you want to comunicate with the server

-- Add User (working) --
function karuchaServerAddUser(f_name, f_nickname, f_email, f_username, f_password, f_permission)

	local karuchaHttpFields = {name = f_name, nickname = f_nickname, email = f_email, username = f_username, password = f_password, permission = f_permission, method = "add_user"}

	local response = httpPOSTRequest(KARUCHA_SERVER_URL, karuchaHttpFields)
	local karuchaJsonResponse = json.decode(response)

	return karuchaJsonResponse["status"]
end


-- Login (working) --
function karuchaServerLogin(f_username, f_password)
	local karuchaHttpFields = {method = "login", username = f_username, password = f_password}

	local response = httpPOSTRequest(KARUCHA_SERVER_URL, karuchaHttpFields)
	local karuchaJsonResponse = json.decode(response)

	if karuchaJsonResponse["status"] == 1 then
		return karuchaJsonResponse['contents']['sess_id']
	else
		return nil
	end
end

-- Logout (working) --
function karuchaServerLogout(f_sess_id)
	local karuchaHttpFields = {method = "logout", sess_id = f_sess_id}
	local response = httpPOSTRequest(KARUCHA_SERVER_URL, karuchaHttpFields)
	local karuchaJsonResponse = json.decode(response)

	return karuchaJsonResponse["status"]
end


-- Open Match (working) --
function karuchaServerOpenMatch(f_sess_id, f_previous_match)
	
	local fields = {sess_id = f_sess_id, method = "open_match"}
	if f_previous_match ~= nil then
		fields["previous_match"] = f_previous_match
	end

	local response = httpPOSTRequest(KARUCHA_SERVER_URL, fields)
	local karuchaJsonResponse = json.decode(response)

	if karuchaJsonResponse["status"] == 1 then
		return karuchaJsonResponse["contents"]["match_id"]
	else
		return 0
	end

end

-- Close Match (working) --
function karuchaServerCloseMatch(f_match_id, f_sess_id)
	local fields = {sess_id = f_sess_id, method = "close_match", match_id = f_match_id}

	local response = httpPOSTRequest(KARUCHA_SERVER_URL, fields)
	local karuchaJsonResponse = json.decode(response)

	return karuchaJsonResponse["status"] 
end


-- Open Stage (working) --
function karuchaServerOpenStage(f_match_id, f_level_number, f_level_mode, f_sess_id)
	local karuchaHttpFields = {method = "open_stage", match_id = f_match_id, level_number = f_level_number, level_mode = f_level_mode, sess_id = f_sess_id}

	local response = httpPOSTRequest(KARUCHA_SERVER_URL, karuchaHttpFields)
	local karuchaJsonResponse = json.decode(response)

	if karuchaJsonResponse["status"] == 1 then
		return karuchaJsonResponse['contents']['stage_id']
	else
		return nil
	end
end


-- Close Stage (working) --
function karuchaServerCloseStage(f_stage_id, f_play_time, f_status, f_sess_id)
	local karuchaHttpFields = {method = "close_stage", stage_id = f_stage_id, play_time = f_play_time, status = f_status, sess_id = f_sess_id}

	local response = httpPOSTRequest(KARUCHA_SERVER_URL, karuchaHttpFields)
	local karuchaJsonResponse = json.decode(response)

	return karuchaJsonResponse["status"]
end


-- End Stage (working) --
function karuchaServerEndStage(f_stage_json, f_sess_id)
	local karuchaHttpFields = {method = "end_stage", stage_json = f_stage_json, sess_id = f_sess_id}

	local response = httpPOSTRequest(KARUCHA_SERVER_URL, karuchaHttpFields)
	local karuchaJsonResponse = json.decode(response)

	return karuchaJsonResponse["status"]
end


-- Add Glyph (working) --
function karuchaServerAddGlyph(f_stage_id, f_glyph_type, f_glyph_column, f_glyph_row, f_glyph_status, f_sess_id)
	local karuchaHttpFields = {
		method = "add_glyph",
		stage_id = f_stage_id,
		glyph_type = f_glyph_type,
		glyph_column = f_glyph_column,
		glyph_row = f_glyph_row,
		status = f_glyph_status,
		sess_id = f_sess_id
	}

	local response = httpPOSTRequest(KARUCHA_SERVER_URL, karuchaHttpFields)
	local karuchaJsonResponse = json.decode(response)

	return karuchaJsonResponse["status"]
end

-- Server Version (working) --
function karuchaServerVersion()
	local karuchaHttpFields = {method = 'version'}

	local response = httpPOSTRequest(KARUCHA_SERVER_URL, karuchaHttpFields)
	local karuchaJsonResponse = json.decode(response)

	if karuchaJsonResponse["status"] == 1 then
		return karuchaJsonResponse['contents']['server_version']
	else
		return nil
	end

end

-- Program example --
function karuchaServerExample()
	local serverVersion = karuchaServerVersion()
	print('Karucha Server - version ' .. serverVersion)
	print()

	local sessId  = karuchaServerLogin('xcalciferus', 'password')
	print('Session ID: ' .. sessId)

	local matchId = karuchaServerOpenMatch(sessId)
	print('New Match: ' .. matchId)

	local stageId = karuchaServerOpenStage(matchId, 1, 3, sessId)
	print('New Stage for match ' .. matchId .. ': ' .. stageId)

	karuchaServerAddGlyph(stageId, 1, 1, 3, 1, sessId)
	karuchaServerAddGlyph(stageId, 1, 1, 1, 1, sessId)
	karuchaServerAddGlyph(stageId, 1, 1, 2, 0, sessId)
	karuchaServerAddGlyph(stageId, 1, 1, 3, 1, sessId)
	karuchaServerAddGlyph(stageId, 1, 1, 2, 1, sessId)
	karuchaServerAddGlyph(stageId, 1, 1, 1, 1, sessId)
	karuchaServerAddGlyph(stageId, 1, 1, 5, 0, sessId)

	local stageStatus = karuchaServerCloseStage(stageId, 60, 1, sessId)
	print('Stage ' .. stageId .. ' close status: ' .. stageStatus)

	local matchStatus = karuchaServerCloseMatch(matchId, sessId)
	print('Match ' .. matchId .. ' close status: ' .. matchStatus)

	local logoutStatus = karuchaServerLogout(sessId)
	print('User logout: ' .. logoutStatus)
end

function karuchaServerEndExample(t_username, t_password)
	local serverVersion = karuchaServerVersion()
	print('Karucha Server - version ' .. serverVersion)
	print()

	local sessId  = karuchaServerLogin(t_username, t_password)
	print('Session ID: ' .. sessId)

	local matchId = karuchaServerOpenMatch(sessId)
	print('New Match: ' .. matchId)

	local stageId = karuchaServerOpenStage(matchId, 1, 3, sessId)
	print('New Stage for match ' .. matchId .. ': ' .. stageId)

	local stageJson = '{"stage_id" : ' .. stageId .. ', "play_time" : 60, "status" : 1, "glyphs":[{"glyph_type" : 1, "glyph_column" : 1, "glyph_row" : 3, "status" : 0}, {"glyph_type" : 1, "glyph_column" : 1, "glyph_row" : 2, "status" : 1}, {"glyph_type" : 1, "glyph_column" : 1, "glyph_row" : 3, "status" : 1}]}'

	local endStatus = karuchaServerEndStage(stageJson, sessId)

	local matchStatus = karuchaServerCloseMatch(matchId, sessId)
	print('Match ' .. matchId .. ' close status: ' .. matchStatus)

	local logoutStatus = karuchaServerLogout(sessId)
	print('User logout: ' .. logoutStatus)
end

--karuchaServerAddUser('Alison', 'XCalciferus', 'alisonlks@outlook.com', 'xcalciferus', 'password', 1)
--karuchaServerEndExample('xcalciferus', 'password')
