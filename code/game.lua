local Game = {}

-- Imports all game modules
local Aux = require "./code/auxiliar"
local Japones = require "./code/japones"
local Kanemy = require "./code/kanemy"
local Menu = require "./code/menu"
local Mouse = require "./code/mouse"
local ResourceDefinitions = require "./code/resource_definitions"
local ResourceManager = require "./code/resource_manager"
local Saves = require "./code/saves"

-- Module variables
local startTime = 0
local gameLevel = startLevelParameter
local oldMaxHP = 100
--local hitsKana = ""
--local hitsBoss = ""
--local kanaWrong = ""
--local bossWrong = ""
local backMainMenu = false
--local callHelpNumber = {0, 0, 0}
--local callHelpString = {"", "", ""}
local message = ""
local flawless = false
--------------------------------------------------------------------
-- start() initializes the game. This should be called from main.lua
--------------------------------------------------------------------
function Game:start()

  -- Do the initial setup
  self:initialize()
  
  -- Game loop
  while loopTrue do ----------------------------- [Game loop]  start
    -- Start the current level
    self:startLevel(gameLevel)

    while loopTrue do --------------------------- [Level loop] start
      self:makeKanemies() -- Generates new kanas (kanemies)
      self:processInput() -- Handles current input
      self:loadLevel()    -- Render the "level: X" string

      if self:zeroLife() then  -- Render the current life, and checks if it's 0
        stageStatus = 0
        -----------------
        -- Karucha Server
        -----------------
        if sessId ~= 0 then Game:GenerateData() local endStatus = karuchaServerEndStage(MOAIJsonParser.encode(json), sessId) end

        action = false
        self:processGameOver() -- If it's zero, it's game over
        action = true
        Game:clearVariable()
        break ------------------------------------ [Level loop] exit
      end

      if self:zeroTime() then -- Render the remaining time, and checks if it's 0
        stageStatus = 1
        -----------------
        -- Karucha Server
        -----------------
        if sessId ~= 0 then Game:GenerateData() local endStatus = karuchaServerEndStage(MOAIJsonParser.encode(json), sessId) end

        action = false
        self:processLevelComplete() -- If it's zero, the level is complete (over)
        action = true
        Game:clearVariable()
        break ------------------------------------ [Game loop]  exit
      end

      coroutine.yield() -- Gives MOAI time to update things

      if paused then    -- checks the game pause status
        pauseTheGame()  -- if pause is On (true), call the pausing function
      end

      if callMainMenu then
        stageStatus = 0
        loopTrue = false
        flawless = false
        self.levelTheme:stop()
        Kanemy:endLevel()
        if backMainMenu then
          --file = assert(io.open("./Logs/User Log " .. dateHour .. ".txt", "a+"), "Error.")
          --file:write("\n     Back to Main Menu\n____________________________\n")
          --file:flush()
          --io.close(file)
          backMainMenu = false
        else
          --Game:saveData()
          -----------------
          -- Karucha Server
          -----------------
          if sessId ~= 0 then Game:GenerateData() local endStatus = karuchaServerEndStage(MOAIJsonParser.encode(json), sessId) end
        end
        -----------------
        -- Karucha Server
        -----------------
        if sessId ~= 0 then matchStatus = karuchaServerCloseMatch(matchId, sessId) print("\nMATCH: " .. matchId .. " CLOSE STATUS: " .. matchStatus .. "\n") end
        
        Game:clearVariable()
      end
    end ------------------------------------------ [Level loop] exit
  end -------------------------------------------- [Game loop]  exit
end

function Game:clearVariable()
  iHitsKana = 0
  iHitsBoss = 0
  --callHelpNumber  = {0, 0, 0}
  iKanaMissed = 0
  iBossMissed = 0
  --hitsKana  = ""
  --hitsBoss  = ""
  --kanaWrong = ""
  --bossWrong = ""
  --callHelpString = {"", "", ""}
  click = 0
  kanaFound = {}
  bossFound = {}
  kanaMissed = {}
  bossMissed = {}
end

function Game:processLevelComplete()
  self.timerInNumber:setString("00") -- Changes time string to two zeros

  Menu:loadMenuScreen("largePanel") if gameLevel > 1 then Game:loadLeftButton("Previous Level") Game:loadCenterButton("Repeat Level") else Game:loadLeftButton("Repeat Level") end Game:loadTopRightButton("Main Menu") if gameLevel == 30 then Game:loadRightButton("New Game") else Game:loadRightButton("Next Level") end

  Kanemy:endLevel() Game:loadAnswer(true)
  
  Saves.save(achieved, "./saves/achievements.sav")

  if Kanemy:maxHP() - Kanemy:currentLife() == 0 then message = "Flawless! Level Completed!" elseif Kanemy:maxHP() - Kanemy:currentLife() == 20 then message = "Great! Level Completed! (Message 1)" elseif Kanemy:maxHP() - Kanemy:currentLife() == 40 then message = "Great! Level Completed! (Message 2)" elseif Kanemy:maxHP() - Kanemy:currentLife() == 60 then message = "Great! Level Completed! (Message 3)" elseif Kanemy:maxHP() - Kanemy:currentLife() == 80 then message = "Great! Level Completed! (Message 4)" elseif Kanemy:maxHP() - Kanemy:currentLife() == 100 then message = "Great! Level Completed! (Message 5)" elseif Kanemy:maxHP() - Kanemy:currentLife() == 120 then message = "Great! Level Completed! (Message 6)" elseif Kanemy:maxHP() - Kanemy:currentLife() == 140 then message = "Great! Level Completed! (Message 7)" end

  if notHitG then
    Game:loadTitle(message, -282, 255)
    Game:loadEndLevelImage(1, 222, 50) Game:loadBonus()
    flawless = true
  else
    Game:loadTitle(message, -282, 255)
    Game:loadEndLevelImage(2, 222, 50)
  end

  local difficulty = gameSave["selected"] --Game:saveData()

  if gameLevel == gameSave[difficulty] then gameSave[difficulty] = gameLevel + 1 Saves.save (gameSave, "./saves/game.sav") end
  
  local notChoosen = true

  while notChoosen do

    if (click == 86 or click == 118) then
      notChoosen = false
      Game:hideLeftButton() if gameLevel > 1 then Game:hideCenterButton() end Game:hideRightButton() Game:hideTopRightButton() midLayer:removeProp(Game.endLevelImage) hudLayer:removeProp(Game.titleText) Game:loadAnswer(false) Menu:unloadMenuScreen()
      if gameLevel > 1 then gameLevel = gameLevel - 1 end
    elseif (click == 66 or click == 98) and gameLevel > 1 then
      notChoosen = false
      Game:hideLeftButton() if gameLevel > 1 then Game:hideCenterButton() end Game:hideRightButton() Game:hideTopRightButton() midLayer:removeProp(Game.endLevelImage) hudLayer:removeProp(Game.titleText) Game:loadAnswer(false) Menu:unloadMenuScreen()
    elseif (click == 78 or click == 110) then
      notChoosen = false
      Game:hideLeftButton() if gameLevel > 1 then Game:hideCenterButton() end Game:hideRightButton() Game:hideTopRightButton() midLayer:removeProp(Game.endLevelImage) hudLayer:removeProp(Game.titleText) Game:loadAnswer(false) Menu:unloadMenuScreen()
      if gameLevel == 30 then gameLevel = 1 else gameLevel = gameLevel + 1 end
    elseif (click == 74 or click == 106) then
      callMainMenu = true
      backMainMenu = true
      notChoosen  = false
      Game:hideLeftButton() if gameLevel > 1 then Game:hideCenterButton() end Game:hideRightButton() Game:hideTopRightButton() midLayer:removeProp(Game.endLevelImage) hudLayer:removeProp(Game.titleText) Game:loadAnswer(false) Menu:unloadMenuScreen()
    end
    
    if Mouse:click() then
      if Mouse:onProp(Game.buttonBox[1]) then
        notChoosen = false
        Game:hideLeftButton() if gameLevel > 1 then Game:hideCenterButton() end Game:hideRightButton() Game:hideTopRightButton() midLayer:removeProp(Game.endLevelImage) hudLayer:removeProp(Game.titleText) Game:loadAnswer(false) Menu:unloadMenuScreen()
        if gameLevel > 1 then gameLevel = gameLevel - 1 end
      elseif Mouse:onProp(Game.buttonBox[2]) and gameLevel > 1 then
        notChoosen = false
        Game:hideLeftButton() if gameLevel > 1 then Game:hideCenterButton() end Game:hideRightButton() Game:hideTopRightButton() midLayer:removeProp(Game.endLevelImage) hudLayer:removeProp(Game.titleText) Game:loadAnswer(false) Menu:unloadMenuScreen()
      elseif Mouse:onProp(Game.buttonBox[3]) then
        notChoosen = false
        Game:hideLeftButton() if gameLevel > 1 then Game:hideCenterButton() end Game:hideRightButton() Game:hideTopRightButton() midLayer:removeProp(Game.endLevelImage) hudLayer:removeProp(Game.titleText) Game:loadAnswer(false) Menu:unloadMenuScreen()
        if gameLevel == 30 then gameLevel = 1 else gameLevel = gameLevel + 1 end
      elseif Mouse:onProp(Game.buttonBox[5]) then
        callMainMenu = true
        backMainMenu = true
        notChoosen  = false
        Game:hideLeftButton() if gameLevel > 1 then Game:hideCenterButton() end Game:hideRightButton() Game:hideTopRightButton() midLayer:removeProp(Game.endLevelImage) hudLayer:removeProp(Game.titleText) Game:loadAnswer(false) Menu:unloadMenuScreen()
      end
    end
    coroutine.yield() -- Gives MOAI time to update things
  end
end

function Game:processGameOver()
  self.lifeInNumber:setString("00") -- Changes time string to two zeros
  
  Menu:loadMenuScreen("largePanel") if gameLevel > 1 then Game:loadLeftButton("Previous Level") Game:loadRightButton("Repeat Level") else Game:loadCenterButton("Repeat Level") end Game:loadTopRightButton("Main Menu")

  Kanemy:endLevel() Game:loadAnswer(true)

  message = "Level Failed! Try again!" Game:loadTitle(message, -282, 255)
  Game:loadEndLevelImage(3, 222, 50)

  self.levelTheme:stop() --Game:saveData()

  local notChoosen = true

  while notChoosen do

    if (click == 86 or click == 118) and gameLevel > 1 then
      notChoosen = false
      Game:initializeLevelTheme() if gameLevel > 1 then Game:hideLeftButton() Game:hideRightButton() else Game:hideCenterButton() end Game:hideTopRightButton() midLayer:removeProp(Game.endLevelImage) hudLayer:removeProp(Game.titleText) Game:loadAnswer(false) Menu:unloadMenuScreen()
      gameLevel  = gameLevel - 1
    elseif ((click == 66 or click == 98) and gameLevel == 1) or ((click == 78 or click == 110) and gameLevel > 1) then
      notChoosen = false
      Game:initializeLevelTheme() if gameLevel > 1 then Game:hideLeftButton() Game:hideRightButton() else Game:hideCenterButton() end Game:hideTopRightButton() midLayer:removeProp(Game.endLevelImage) hudLayer:removeProp(Game.titleText) Game:loadAnswer(false) Menu:unloadMenuScreen()
    elseif (click == 74 or click == 106) then
      callMainMenu = true
      backMainMenu = true
      notChoosen  = false
      if gameLevel > 1 then Game:hideLeftButton() Game:hideRightButton() else Game:hideCenterButton() end Game:hideTopRightButton() midLayer:removeProp(Game.endLevelImage) hudLayer:removeProp(Game.titleText) Game:loadAnswer(false) Menu:unloadMenuScreen()
    end

    if Mouse:click() then
      if Mouse:onProp(Game.buttonBox[1]) and gameLevel > 1 then
        notChoosen = false
        Game:initializeLevelTheme() if gameLevel > 1 then Game:hideLeftButton() Game:hideRightButton() else Game:hideCenterButton() end Game:hideTopRightButton() midLayer:removeProp(Game.endLevelImage) hudLayer:removeProp(Game.titleText) Game:loadAnswer(false) Menu:unloadMenuScreen()
        gameLevel  = gameLevel - 1
      elseif (Mouse:onProp(Game.buttonBox[2]) and gameLevel == 1) or (Mouse:onProp(Game.buttonBox[3]) and gameLevel > 1) then
        notChoosen = false
        Game:initializeLevelTheme() if gameLevel > 1 then Game:hideLeftButton() Game:hideRightButton() else Game:hideCenterButton() end Game:hideTopRightButton() midLayer:removeProp(Game.endLevelImage) hudLayer:removeProp(Game.titleText) Game:loadAnswer(false) Menu:unloadMenuScreen()
      elseif Mouse:onProp(Game.buttonBox[5]) then
        callMainMenu = true
        backMainMenu = true
        notChoosen  = false
        if gameLevel > 1 then Game:hideLeftButton() Game:hideRightButton() else Game:hideCenterButton() end Game:hideTopRightButton() midLayer:removeProp(Game.endLevelImage) hudLayer:removeProp(Game.titleText) Game:loadAnswer(false) Menu:unloadMenuScreen()
      end
    end
    coroutine.yield() -- Gives MOAI time to update things
  end
end

function Game:loadAnswer(show)
  if show then
    
    if (gameLevel % 3 == 0) and (gameLevel % 15 ~= 0) then Game:loadKanaAnswer(-222) Game:loadBossAnswer(0) elseif gameLevel % 15 == 0 then Game:loadBossAnswer(-87) else Game:loadKanaAnswer(-87) end
  
  else

    if (gameLevel % 3 == 0) and (gameLevel % 15 ~= 0) then Game:hideKanaAnswer() Game:hideBossAnswer() elseif gameLevel % 15 == 0 then Game:hideBossAnswer() else Game:hideKanaAnswer() end

  end
end

function Game:initializeLevelTheme()
  if gameSave["sound"] == 1 then
    Game.levelTheme:play()
  end
end
-------------------------------------------------------
-- initialize() does all the initial setup for the game
-------------------------------------------------------
function Game:initialize()
  startTime = 0
  gameLevel = startLevelParameter
  dMode = gameSave["selected"]

  -- We load all our resources
  ResourceDefinitions:setDefinitions(resource_definitions)
  -- Initialize fonts
  self:initializeFonts()
  -- Initialize input/Japonese manager
  Japones:initialize()

  self.shotSound = ResourceManager:get("shotSound") self.levelTheme = ResourceManager:get("levelTheme") self.bonusSound = ResourceManager:get("bonusSound")

  Game:initializeLevelTheme()
  -----------------
  -- Karucha Server
  -----------------
  if sessId ~= 0 then matchId = karuchaServerOpenMatch(sessId) print("\nNEW MATCH: " .. matchId .. "\n") end

end

function Game:initializeFonts()
  kanjiFont1 = ResourceManager:get("kanjiFont1") numbersFont2 = ResourceManager:get("numbersFont2") numbersFont1 = ResourceManager:get("numbersFont1") romanjiFont1 = ResourceManager:get("romanjiFont1") textFont1 = ResourceManager:get("textFont1")
end

function Game:startLevel(level)
  MOAIRenderMgr.setRenderTable(nil)
  -- First of all, we start all layers
  backgroundLayer = MOAILayer2D.new()
  backgroundLayer:setViewport(viewport)
  -- midLayer
  midLayer = MOAILayer2D.new()
  midLayer:setViewport(viewport)
  -- hudLayer
  hudLayer = MOAILayer2D.new()
  hudLayer:setViewport(viewport)

  self:loadBackground()
  self:loadHud()

  -- Initialize Kanemy module
  Kanemy:initialize(level,flawless)

  Game:initializeLife()
  Game:initializeTimer()
  Game:initializeLevel()
  Game:initializeBonus()
  Game:initializeButtons()

  MOAIRenderMgr.setRenderTable({backgroundLayer, midLayer, hudLayer})
  Kanemy.startLevel()
  -----------------
  -- Karucha Server
  -----------------
  if sessId ~= 0 and not callMainMenu then stageId = karuchaServerOpenStage(matchId, gameLevel, stageModes[dMode], sessId) print("\nNEW STAGE FOR MATCH " .. matchId .. ": " .. stageId .. "\n") end
end

function Game:initializeBonus()
  self.bonus = MOAIProp2D.new()
  self.bonus:setDeck(ResourceManager:get("bonus"))  
  setDefaultPos(self.bonus, "bonus")
  self.bonus:setBlendMode(MOAIProp.GL_SRC_ALPHA, MOAIProp.GL_ONE_MINUS_SRC_ALPHA)
  self.bonus:setColor(1, 1, 1, 0)
  hudLayer:insertProp(self.bonus)
end

function Game:initializeButtons()

  local box = {"leftBox", "centerBox", "rightBox", "topLeftBox", "topRightBox"}

  self.buttonBox = {}
  self.buttonImg = {}

  for i = 1, 5 do
    self.buttonBox[i] = MOAIProp2D.new()
    self.buttonBox[i]:setDeck(ResourceManager:get(box[i]))
    setDefaultPos(self.buttonBox[i], box[i])
    self.buttonBox[i]:setBlendMode(MOAIProp.GL_SRC_ALPHA, MOAIProp.GL_ONE_MINUS_SRC_ALPHA)
    self.buttonBox[i]:setColor(1, 1, 1, 0)

    self.buttonImg[i] = MOAIProp2D.new()
    self.buttonImg[i]:setDeck(ResourceManager:get("button"))
  end
end

function Game:loadEndLevelImage(index, x, y)

  local image = {"flawless", "won", "gameover"}

  self.endLevelImage = MOAIProp2D.new()
  self.endLevelImage:setDeck(ResourceManager:get(image[index]))
  self.endLevelImage:setLoc(x, y)
  self.endLevelImage:setScl(0.27, 0.27)
  midLayer:insertProp(self.endLevelImage)
end

function Game:loadLeftButton(text)

  local x = -222 local y = -79 self.buttonImg[1]:setLoc(x, y)

  hudLayer:insertProp(self.buttonBox[1]) hudLayer:insertProp(self.buttonImg[1]) hudLayer:insertProp(self.buttonText[1])

  self.buttonText[1]:setLoc(x - 112, y - 38)
  self.buttonText[1]:setString(text)
end

function Game:loadCenterButton(text)

  local x = 0 local y = -79 self.buttonImg[2]:setLoc(x, y)

  hudLayer:insertProp(self.buttonBox[2]) hudLayer:insertProp(self.buttonImg[2]) hudLayer:insertProp(self.buttonText[2])

  self.buttonText[2]:setLoc(x - 112, y - 38)
  self.buttonText[2]:setString(text)
end

function Game:loadRightButton(text)
  
  local x = 222 local y = -79 self.buttonImg[3]:setLoc(x, y)

  hudLayer:insertProp(self.buttonBox[3]) hudLayer:insertProp(self.buttonImg[3]) hudLayer:insertProp(self.buttonText[3])

  self.buttonText[3]:setLoc(x - 112, y - 38)
  self.buttonText[3]:setString(text)
end

function Game:loadTopLeftButton(text)
  
  local x = -222 local y = 220 self.buttonImg[4]:setLoc(x, y)

  hudLayer:insertProp(self.buttonBox[4]) hudLayer:insertProp(self.buttonImg[4]) hudLayer:insertProp(self.buttonText[4])

  self.buttonText[4]:setLoc(x - 112, y - 38)
  self.buttonText[4]:setString(text)
end

function Game:loadTopRightButton(text)
  
  local x = 222 local y = 220 self.buttonImg[5]:setLoc(x, y)

  hudLayer:insertProp(self.buttonBox[5]) hudLayer:insertProp(self.buttonImg[5]) hudLayer:insertProp(self.buttonText[5])

  self.buttonText[5]:setLoc(x - 112, y - 38)
  self.buttonText[5]:setString(text)
end

function Game:hideLeftButton()
  hudLayer:removeProp(Game.buttonBox[1]) hudLayer:removeProp(Game.buttonImg[1]) hudLayer:removeProp(Game.buttonText[1])
end

function Game:hideCenterButton()
  hudLayer:removeProp(Game.buttonBox[2]) hudLayer:removeProp(Game.buttonImg[2]) hudLayer:removeProp(Game.buttonText[2])
end

function Game:hideRightButton()
  hudLayer:removeProp(Game.buttonBox[3]) hudLayer:removeProp(Game.buttonImg[3]) hudLayer:removeProp(Game.buttonText[3])
end

function Game:hideTopLeftButton()
  hudLayer:removeProp(Game.buttonBox[4]) hudLayer:removeProp(Game.buttonImg[4]) hudLayer:removeProp(Game.buttonText[4])
end

function Game:hideTopRightButton()
  hudLayer:removeProp(Game.buttonBox[5]) hudLayer:removeProp(Game.buttonImg[5]) hudLayer:removeProp(Game.buttonText[5])
end

function Game:loadTitle(text, x, y)
  hudLayer:insertProp(self.titleText) self.titleText:setColor(132, 134, 135) self.titleText:setLoc(x, y) self.titleText:setString(text)
end

function Game:loadBonus()
  if self.bonus then
    if gameSave["sound"] == 1 then
      Game.bonusSound:play()
    end
    self.bonus.thread = MOAIThread.new()
    self.bonus.thread:run(Game.bonusAlpha, self.bonus)
  else
    print("Bonus prop not found!")
  end
end

function Game.bonusAlpha(lprop)
  MOAIThread.blockOnAction(lprop:seekColor(1, 1, 1, 1, 1, MOAIEaseType.LINEAR))
  sleep(1)
  MOAIThread.blockOnAction(lprop:seekColor(1, 1, 1, 0, 1, MOAIEaseType.LINEAR))
  lprop.thread:stop()
end

--function Game:saveData()
--  file = assert(io.open("./Logs/User Log " .. dateHour .. ".txt", "a+"), "Error.")

--  callHelpString[1] = "\n\nUse of Help: " .. tostring(callHelpNumber[1]) if dMode ~= "hard" then callHelpString[2] = "\n\nUse of Kanas Help: " .. tostring(callHelpNumber[2]) callHelpString[3] = "\n\nUse of Bosses Help: " .. tostring(callHelpNumber[3]) end

--  Game:concatenateString()

--  if gameLevel % 3 == 0 and gameLevel % 15 ~= 0 then
--    file:write("\nLevel: " .. tostring(gameLevel) .. "\n\nMode: " .. dMode .. callHelpString[1] .. callHelpString[2] .. callHelpString[3] .. "\n\nHits Kana: " .. tostring(iHitsKana) .. "\n" .. hitsKana .. "\n\nHits Boss: " .. tostring(iHitsBoss) .. "\n" .. hitsBoss .. "\n\nKanas Lost: " .. tostring(iKanaMissed) .. "\n" .. kanaWrong .. "\n\nBosses Lost: " .. tostring(iBossMissed) .. "\n" .. bossWrong)
--  elseif gameLevel % 15 == 0 then
--    file:write("\nLevel: " .. tostring(gameLevel) .. "\n\nMode: " .. dMode .. callHelpString[1] .. callHelpString[3] .. "\n\nHits Boss: " .. tostring(iHitsBoss) .. "\n" .. hitsBoss .. "\n\nBosses Lost: " .. tostring(iBossMissed) .. "\n" .. bossWrong)
--  else
--    file:write("\nLevel: " .. tostring(gameLevel) .. "\n\nMode: " .. dMode .. callHelpString[1] .. callHelpString[2] .. "\n\nHits Kana: " .. tostring(iHitsKana) .. "\n" .. hitsKana .. "\n\nKanas Lost: " .. tostring(iKanaMissed) .. "\n" .. kanaWrong)
--  end
  
--  if callMainMenu then
--    file:write("\n\nStatus:\n\n" .. "Level Stopped in " .. Game.secs .. " Seconds\n____________________________\n")
--  else
--    file:write("\n\nStatus:\n\n" .. message .. "\n____________________________\n")
--  end

--  file:flush()
--  io.close(file)
--end

--function Game:concatenateString()

--  for i = 1, iHitsKana do if i == 1 then hitsKana = hitsKana .. kanaFound[i] else hitsKana = hitsKana .. ", " .. kanaFound[i] end end

--  for i = 1, iHitsBoss do if i == 1 then hitsBoss = hitsBoss .. bossFound[i] else hitsBoss = hitsBoss .. ", " .. bossFound[i] end end

--  for i = 1, iKanaMissed do if i == 1 then kanaWrong = kanaWrong .. kanaMissed[i] else kanaWrong = kanaWrong .. ", " .. kanaMissed[i] end end

--  for i = 1, iBossMissed do if i == 1 then bossWrong = bossWrong .. bossMissed[i] else bossWrong = bossWrong .. ", " .. bossMissed[i] end end

--end

function Game:GenerateData()
  
  json = {stage_id = stageId, play_time = 60 - Game.secs, status = stageStatus} content = {}

  local increment = 0

  if #kanaFound > 0 then

    for i = 1, #kanaFound do

      for j = 1, 28 do

        for k = 1, #kanaLevel[j] do

          if kanaLevel[j][k] == kanaFound[i] then

            increment = increment + 1 content[increment] = {glyph_column = k, glyph_row = j, status = 1, glyph_type = 0}

          end
        end
      end
    end
  end

  if #kanaMissed > 0 then

    for i = 1, #kanaMissed do

      for j = 1, 28 do

        for k = 1, #kanaLevel[j] do

          if kanaLevel[j][k] == kanaMissed[i] then

            increment = increment + 1 content[increment] = {glyph_column = k, glyph_row = j, status = 0, glyph_type = 0}

          end
        end
      end
    end
  end

  if #bossFound > 0 then

    for i = 1, #bossFound do

      for j = 1, 6 do

        for k = 1, #bossLevel[j] do

          if bossLevel[j][k] == bossFound[i] then

            increment = increment + 1 content[increment] = {glyph_column = k, glyph_row = j, status = 1, glyph_type = 1}

          end
        end
      end
    end
  end

  if #bossMissed > 0 then

    for i = 1, #bossMissed do

      for j = 1, 6 do

        for k = 1, #bossLevel[j] do

          if bossLevel[j][k] == bossMissed[i] then

            increment = increment + 1 content[increment] = {glyph_column = k, glyph_row = j, status = 0, glyph_type = 1}

          end
        end
      end
    end
  end

  json["glyphs"] = content

end

function Game:initializeLife()
  self.barra = MOAIProp2D.new()
  self.barra:setDeck(ResourceManager:get("barra"))  
  setDefaultPos(self.barra, "barra")
  hudLayer:insertProp(self.barra)

  self.lifeInNumber = MOAITextBox.new()
  self.lifeInNumber:setFont(numbersFont2)
  self.lifeInNumber:setYFlip(true)
  self.lifeInNumber:setRect(-75, -35, 75, 35)
  Aux.centralize(self.lifeInNumber)
  hudLayer:insertProp(Game.lifeInNumber)
end

function Game:initializeTimer()
  self.timerInNumber = MOAITextBox.new()
  self.timerInNumber:setFont(numbersFont2)
  self.timerInNumber:setYFlip(true)
  self.timerInNumber:setRect(-115, -50, 115, 50)
  Aux.centralize(Game.timerInNumber)
  hudLayer:insertProp(Game.timerInNumber)
  startTime = os.time()
    self.currentTime = timeOnEachLevel
end

function Game:initializeLevel()
  self.levelInNumber = MOAITextBox.new()
  self.levelInNumber:setFont(numbersFont2)
  self.levelInNumber:setYFlip(true)
  self.levelInNumber:setRect(-50, -50, 50, 50)
  Aux.centralize(self.levelInNumber)
  hudLayer:insertProp(Game.levelInNumber)

  self.levelText = MOAITextBox.new()
  self.levelText:setFont(textFont1)
  self.levelText:setYFlip(true)
  self.levelText:setRect(-150, -50, 150, 50)
  Aux.centralize(self.levelText)
  hudLayer:insertProp(Game.levelText)

  self.modeText = MOAITextBox.new()
  self.modeText:setFont(textFont1)
  self.modeText:setYFlip(true)
  self.modeText:setRect(-150, -50, 150, 50)
  Aux.centralize(self.modeText)
  hudLayer:insertProp(Game.modeText)

  self.titleText = MOAITextBox.new()
  self.titleText:setFont(textFont1)
  self.titleText:setYFlip(true)
  self.titleText:setRect(0, 0, 448, 66)
  self.titleText:setAlignment(MOAITextBox.LEFT_JUSTIFY)

  self.buttonText = {}

  for i = 1, 5 do
    self.buttonText[i] = MOAITextBox.new()
    self.buttonText[i]:setFont(numbersFont1)
    self.buttonText[i]:setRect(0, 0, 224, 66)
    Aux.centralize(self.buttonText[i])

    self.buttonText[i]:setColor(132, 134, 135)
    self.buttonText[i]:setYFlip(true)
  end
end

function Game:zeroLife()
  local current = Kanemy:currentLife()
  local total = Kanemy:maxHP()

  if total > oldMaxHP then oldMaxHP = total end
  
  local tx = "" .. current
  local xOffset = (1 - current/total)/2

  self.lifeInNumber:setColor(0, 0, 0)
  self.lifeInNumber:setLoc(LEFT + 10 , TOP - 50)
  
  self.barra:setScl(current/total, 1, 1)
  self.barra:setLoc(self.barra.defaultX - (xOffset * barWidth), self.barra.defaultY)
  return Kanemy:currentLife() < 1
end

function Game:zeroTime()
  local tx = ""
  self.secs = math.floor(self.currentTime - (os.time() - startTime))
  tx = tx .. self.secs
  if #tx == 1 then
    tx = "0" .. tx
  end
  self.timerInNumber:setString(tx)
  self.timerInNumber:setColor(160, 160, 158)
  setWinPos(self.timerInNumber, 514, 35)
  return self.secs <= 0
end

function Game:loadLevel()
  local tx = ""
  tx = tx .. gameLevel
  self.levelInNumber:setString(tx)
  self.levelInNumber:setColor(132, 134, 135)
  setWinPos(self.levelInNumber, 840, 35)

  self.levelText:setString("level")
  self.levelText:setColor(132, 134, 135)
  setWinPos(self.levelText, 780, 25)

  self.modeText:setString(dMode)
  self.modeText:setColor(132, 134, 135)
  if dMode == "easy" then
    setWinPos(self.modeText, 640, 20)
  else
    setWinPos(self.modeText, 640, 25)
  end
end

function Game:loadBackground()
    local prop = MOAIProp2D.new()
    local dk = nil
    currentDk = math.floor((gameLevel - 1)/3) % 3
    if currentDk == 0 then
      dk = ResourceManager:get("manha")
    elseif currentDk == 1 then
      dk = ResourceManager:get("tarde")
    elseif currentDk == 2 then
      dk = ResourceManager:get("noite")
    end
    prop:setDeck(dk)
    setDefaultPos(prop, "manha") -- tanto faz aqui, manha/tarde/noite = mesmo tamanho
    self.cannon = MOAIProp.new()
    self.cannon:setDeck(ResourceManager:get("cannon"))
    setDefaultPos(self.cannon, "cannon")
    self.cannon.rotate = true
    backgroundLayer:insertProp(prop)
    backgroundLayer:insertProp(self.cannon)
end

function Game:loadHud()
    local prop  = MOAIProp2D.new()
    local hudDK = ResourceManager:get("hud")
    prop:setDeck(hudDK)
    setDefaultPos(prop, "hud")
    hudLayer:insertProp(prop)
end
-------------------------------------------------------
-- processInput() talks to InputManager to handle input
-------------------------------------------------------
function Game:processInput()
  local kn = Japones:getKana()
  
  if kn then
    Game:newLaser(kn)
  end

  if Mouse:click() then
    lx, ly = Mouse:position()
    print(midLayer:wndToWorld(lx, ly))
  end
end

function Game:makeKanemies()
  Kanemy:makeInTime()
end

function Game:newLaser(kn)
  local laserProp = MOAIProp.new()
  laserProp:setDeck(ResourceManager:get("raio"))
  local lx, ly = self.cannon:getLoc()
  laserProp:setLoc(lx, ly, 1)
  laserProp.target = kn
  laserProp.rotate = true
  midLayer:insertProp(laserProp)
  laserProp.thread = MOAIThread.new()
  laserProp.thread:run(Game.laserShot, laserProp)
end

function Game.laserShot(lprop)
  local laserSpeed = 1.5
  local kanaMeteor = Kanemy:getKanaProp(lprop.target)
  local capturaProp = MOAIProp.new()
  if kanaMeteor then
    if gameSave["sound"] == 1 then
      Game.shotSound:play()
    end

    local kana = kanaMeteor
    local ship = false

    if Japones:isBoss(kanaMeteor.kana) then kana = kanaMeteor.bossProp end

    for i = 1, 28 do for j = 1, #kanaLevel[i] do if kanaLevel[i][j] == kanaMeteor.kana then ship = true end end end

    if ship then
      iHitsKana = iHitsKana + 1 kanaFound[iHitsKana] = kanaMeteor.kana ship = false
    else
      iHitsBoss = iHitsBoss + 1 bossFound[iHitsBoss] = kanaMeteor.kana ship = false
    end

    kanaMeteor.stop = true

    local tx, ty = kana:getLoc()
    local lx, ly = lprop:getLoc()
    
    local dist = Aux.distance(lx, ly, tx, ty)
    local lScale = dist/525

    lprop:setScl(1, lScale, 1)

    Aux.setRot(Game.cannon, tx, ty)
    Aux.setRot(lprop, tx, ty)

    capturaProp:setDeck(ResourceManager:get("capturaNave"))
    capturaProp:setLoc(tx, ty, 1)
    local sx, sy, sz = kana:getScl()
    capturaProp:setScl(sx, sy, sz)
    midLayer:insertProp(capturaProp)

    MOAIThread.blockOnAction(lprop:seekColor(1, 1, 1, 0, 0.25, MOAIEaseType.LINEAR))
    midLayer:removeProp(lprop)

    local actionGroup = MOAIAction.new()
    local fadeCircle  = capturaProp:seekColor(1, 1, 1, 0, 0.5, MOAIEaseType.LINEAR)
    local fadeKana    = kana:seekColor(1, 1, 1, 0, 0.5, MOAIEaseType.LINEAR)
    local fadeShip    = kanaMeteor.shipProp:seekColor(1, 1, 1, 0, 0.5, MOAIEaseType.LINEAR)
    
    actionGroup:addChild(fadeCircle)
    actionGroup:addChild(fadeKana)
    actionGroup:addChild(fadeShip)

    actionGroup:start()
    MOAIThread.blockOnAction(actionGroup)

    Kanemy.destroy(kanaMeteor.kana, false)
  end
  midLayer:removeProp(capturaProp)
  lprop.thread:stop()
  midLayer:removeProp(lprop)
end
-----------------------------------------------------------------------------
-- sleepCoroutine(time) helper method to freeze the thread for "time" seconds
-----------------------------------------------------------------------------
function sleepCoroutine(time)
  local timer = MOAITimer.new()
  timer:setSpan(time)
  timer:start()
  MOAICoroutine.blockOnAction(timer)
end

function sleep(time)
  local st = os.time()
  while os.time() - st < time do
    coroutine.yield()
  end
end

function pauseTheGame()
  Game.oldRoot = MOAIActionMgr.getRoot() -- Get the root 
  MOAIActionMgr.setRoot () -- Clear out the old root; Will be automatically recreated
  Menu:loadMenuScreen("largePanel")
  local index, windowIndex = 0, 1
  local limit = 0
  local window = {}
  if not callGameHelp and not callKanaHelp and not callBossHelp then
    Game:loadTitle("Pause", -282, 255)
    Game:loadLeftButton("Resume") Game:loadRightButton("Main Menu")
    Game.levelTheme:pause()
  else
    Game:loadCenterButton("Resume")
    if callGameHelp then
      Game:loadTitle("Help", -282, 255)
      --callHelpNumber[1] = callHelpNumber[1] + 1
    else
      local search = true
      if callKanaHelp then
        while search do
          if helpLevel[1][index] == gameLevel then limit = index search = false else index = index + 1 end
        end
        if dMode == "easy" and index > 1 then Game:loadTopLeftButton("Level " .. tostring(helpLevel[1][index - 1])) end
        Game:loadTitle("Kanas Context-sensitive Help", -282, 255) Game:loadKanaHelp(index)
        --callHelpNumber[2] = callHelpNumber[2] + 1
      else
        while search do
          if helpLevel[2][index] == gameLevel then limit = index search = false else index = index + 1 end
        end
        if dMode == "easy" and index > 1 then Game:loadTopLeftButton("Level " .. tostring(helpLevel[2][index - 1])) end

        local number, bosses = 1, #bossLevel[index]
        local search = true
        while search do
          if bosses > 4 then window[number] = 4 number = number + 1 bosses = bosses - 4 else window[number] = bosses search = false end
        end

        Game:loadTitle("Bosses Context-sensitive Help", -282, 255) Game:loadBossHelp(index, 1, window[1]) if callBossHelp and #bossLevel[index] > 4 then Game:loadRightButton("Page " .. tostring(windowIndex + 1)) end
        --callHelpNumber[3] = callHelpNumber[3] + 1
      end
    end
  end

  function pointerCallbackHelp(x, y)
    pointerX, pointerY = x, y
  end

  function clickCallbackHelp(down)
    recalculateWindows()

    if paused and down then
      if not callGameHelp and not callKanaHelp and not callBossHelp then
        if onPropHelp(Game.buttonBox[1]) or notInAreaHelp() then
          Game:hideLeftButton() Game:hideRightButton()
          unpauseTheGame()
        elseif onPropHelp(Game.buttonBox[3]) then
          callMainMenu = true
          Game:hideLeftButton() Game:hideRightButton()
          unpauseTheGame()
        end
      elseif not callGameHelp and dMode == "easy" then
        if onPropHelp(Game.buttonBox[2]) or notInAreaHelp() then
          hideTopButtonHelp()
          Game:hideCenterButton()
          if callKanaHelp then Game:hideKanaHelp(index) else Game:hideBossHelp(index, window[windowIndex]) end
          unpauseTheGame()
        elseif onPropHelp(Game.buttonBox[4]) and index >   1   then
          hideTopButtonHelp() index = index - 1 if callKanaHelp then Game:hideKanaHelp(index + 1) Game:loadKanaHelp(index) else Game:hideBossHelp(index + 1, window[windowIndex]) Game:loadBossHelp(index, 1, window[1]) end
          loadTopButtonHelp()
        elseif onPropHelp(Game.buttonBox[5]) and index < limit then
          hideTopButtonHelp() index = index + 1 if callKanaHelp then Game:hideKanaHelp(index - 1) Game:loadKanaHelp(index) else Game:hideBossHelp(index - 1, window[windowIndex]) Game:loadBossHelp(index, 1, window[1]) end
          loadTopButtonHelp()
        elseif onPropHelp(Game.buttonBox[1]) and windowIndex >    1    then
          hideButtonBossHelp() windowIndex = windowIndex - 1 if callBossHelp then Game:hideBossHelp(index, window[windowIndex + 1]) Game:loadBossHelp(index, windowIndex, window[windowIndex]) end
          loadButtonBossHelp()
        elseif onPropHelp(Game.buttonBox[3]) and windowIndex < #window then
          hideButtonBossHelp() windowIndex = windowIndex + 1 if callBossHelp then Game:hideBossHelp(index, window[windowIndex - 1]) Game:loadBossHelp(index, windowIndex, window[windowIndex]) end
          loadButtonBossHelp()
        end
      elseif not callGameHelp and dMode == "medium" then
        if onPropHelp(Game.buttonBox[2]) or notInAreaHelp() then
          hideTopButtonHelp()
          Game:hideCenterButton()
          if callKanaHelp then Game:hideKanaHelp(index) else Game:hideBossHelp(index, window[windowIndex]) end
          unpauseTheGame()
        elseif onPropHelp(Game.buttonBox[1]) and windowIndex >    1    then
          hideButtonBossHelp() windowIndex = windowIndex - 1 if callBossHelp then Game:hideBossHelp(index, window[windowIndex + 1]) Game:loadBossHelp(index, windowIndex, window[windowIndex]) end
          loadButtonBossHelp()
        elseif onPropHelp(Game.buttonBox[3]) and windowIndex < #window then
          hideButtonBossHelp() windowIndex = windowIndex + 1 if callBossHelp then Game:hideBossHelp(index, window[windowIndex - 1]) Game:loadBossHelp(index, windowIndex, window[windowIndex]) end
          loadButtonBossHelp()
        end
      else
        if onPropHelp(Game.buttonBox[2]) or notInAreaHelp() then
          Game:hideCenterButton()
          unpauseTheGame()
        end
      end
    end
  end

  function recalculateWindows()
    if callBossHelp then
      window = {}
      local number, bosses = 1, #bossLevel[index]
      local search = true
      while search do
        if bosses > 4 then window[number] = 4 number = number + 1 bosses = bosses - 4 else window[number] = bosses search = false end
      end
    end
  end

  function loadButtonBossHelp()
    if windowIndex > 1 then
      if callBossHelp then Game:loadLeftButton("Page " .. tostring(windowIndex - 1)) end
    end
    if windowIndex < #window then
      if callBossHelp then Game:loadRightButton("Page " .. tostring(windowIndex + 1)) end
    end
  end

  function hideButtonBossHelp()
    if windowIndex > 1 then Game:hideLeftButton() end
    if windowIndex < #window then Game:hideRightButton() end
  end

  function loadTopButtonHelp()
    if callBossHelp then
      if windowIndex == 1 then
        windowIndex = 1 Game:hideRightButton() if #bossLevel[index] > 4 then Game:loadRightButton("Page " .. tostring(windowIndex + 1)) end
      elseif windowIndex > 1 and windowIndex < #window then
        windowIndex = 1 Game:hideLeftButton()  if #bossLevel[index] > 4 then Game:hideRightButton() Game:loadRightButton("Page " .. tostring(windowIndex + 1)) end
      elseif windowIndex == #window then
        windowIndex = 1 Game:hideLeftButton()  if #bossLevel[index] > 4 then Game:loadRightButton("Page " .. tostring(windowIndex + 1)) end
      end
    end

    if dMode == "easy" and index > 1 then
      if callKanaHelp then Game:loadTopLeftButton("Level " .. tostring(helpLevel[1][index - 1])) else Game:loadTopLeftButton("Level " .. tostring(helpLevel[2][index - 1])) end
    end
    if dMode == "easy" and index < limit then
      if callKanaHelp then Game:loadTopRightButton("Level " .. tostring(helpLevel[1][index + 1])) else Game:loadTopRightButton("Level " .. tostring(helpLevel[2][index + 1])) end
    end
  end

  function hideTopButtonHelp()
    if callBossHelp then
      if windowIndex == 1 then
        if #bossLevel[index] > 4 then Game:hideRightButton() end
      elseif windowIndex > 1 and windowIndex < #window then 
        if #bossLevel[index] > 4 then Game:hideLeftButton() Game:hideRightButton() end
      elseif windowIndex == #window then
        if #bossLevel[index] > 4 then Game:hideLeftButton() end
      end
    end

    if dMode == "easy" and index > 1 then Game:hideTopLeftButton() end
    if dMode == "easy" and index < limit then Game:hideTopRightButton() end
  end

  function worldPositionHelp()
    local mouseX, mouseY = hudLayer:wndToWorld(pointerX, pointerY)
    return mouseX, mouseY
  end

  function onPropHelp(prop)
    if prop == nil then return false end
    local worldX, worldY = worldPositionHelp()
    return prop:inside(worldX, worldY)
  end

  function notInAreaHelp()
    local worldX, worldY = worldPositionHelp()
    return worldX < -333 or worldX > 333 or worldY < -110
  end

  if MOAIInputMgr.device.pointer then
    -- mouse input
    MOAIInputMgr.device.pointer:setCallback(pointerCallbackHelp)
    MOAIInputMgr.device.mouseLeft:setCallback(clickCallbackHelp)
  else
    -- touch input
    MOAIInputMgr.device.touch:setCallback(
      function(eventType, idx, x, y, tapCount)
        pointerCallback(x, y)
        if eventType == MOAITouchSensor.TOUCH_DOWN then
          clickCallbackHelp(true)
        elseif eventType == MOAITouchSensor.TOUCH_UP then
          clickCallbackHelp(false)
        end
      end
    )
  end

  MOAIInputMgr.device.keyboard:setCallback(
    function(key, down)
      if down == true then
        recalculateWindows()
        if key == 27 then
          if not callGameHelp and not callKanaHelp and not callBossHelp then
            Game:hideLeftButton() Game:hideRightButton()
            unpauseTheGame()
          elseif not callGameHelp and (dMode == "easy" or dMode == "medium") then
            hideTopButtonHelp()
            Game:hideCenterButton()
            if callKanaHelp then Game:hideKanaHelp(index) else Game:hideBossHelp(index, window[windowIndex]) end
            unpauseTheGame()
          else
            Game:hideCenterButton()
            unpauseTheGame()
          end
        elseif (key == 78 or key == 110) and not callGameHelp and not callKanaHelp and not callBossHelp then
            callMainMenu = true
            Game:hideLeftButton() Game:hideRightButton()
            unpauseTheGame()
        elseif (key == 70 or key == 102) and not callGameHelp and dMode == "easy" and index >   1   then
          hideTopButtonHelp() index = index - 1 if callKanaHelp then Game:hideKanaHelp(index + 1) Game:loadKanaHelp(index) else Game:hideBossHelp(index + 1, window[windowIndex]) Game:loadBossHelp(index, 1, window[1]) end
          loadTopButtonHelp()
        elseif (key == 74 or key == 106) and not callGameHelp and dMode == "easy" and index < limit then
          hideTopButtonHelp() index = index + 1 if callKanaHelp then Game:hideKanaHelp(index - 1) Game:loadKanaHelp(index) else Game:hideBossHelp(index - 1, window[windowIndex]) Game:loadBossHelp(index, 1, window[1]) end
          loadTopButtonHelp()
        elseif (key == 86 or key == 118) and not callGameHelp and (dMode == "easy" or dMode == "medium") and windowIndex >    1    then
          hideButtonBossHelp() windowIndex = windowIndex - 1 if callBossHelp then Game:hideBossHelp(index, window[windowIndex + 1]) Game:loadBossHelp(index, windowIndex, window[windowIndex]) end
          loadButtonBossHelp()
        elseif (key == 78 or key == 110) and not callGameHelp and (dMode == "easy" or dMode == "medium") and windowIndex < #window then
          hideButtonBossHelp() windowIndex = windowIndex + 1 if callBossHelp then Game:hideBossHelp(index, window[windowIndex - 1]) Game:loadBossHelp(index, windowIndex, window[windowIndex]) end
          loadButtonBossHelp()
        end
      end
    end
  )
end

function unpauseTheGame()
  MOAIActionMgr.setRoot(Game.oldRoot)
  Japones:initialize()
  Mouse:initialize()

  if not callGameHelp and not callKanaHelp and not callBossHelp then Game:initializeLevelTheme() end

  startTime = os.time()
  Game.currentTime = Game.secs
  paused = false
  Kanemy:updateLastTime()

  hudLayer:removeProp(Game.titleText)
  Menu:unloadMenuScreen()
  callGameHelp = false
  callBossHelp = false
  callKanaHelp = false
end

function Game:loadKanaAnswer(position)
  self.kanaAnswerText = {}

  local Xaxis, Yaxis = 0, 0

  for i = 1, iKanaMissed*2 + 2 do

    self.kanaAnswerText[i] = MOAITextBox.new()
    self.kanaAnswerText[i]:setRect(0, 0, 224, 66)

    hudLayer:insertProp(self.kanaAnswerText[i])

    self.kanaAnswerText[i]:setColor(132, 134, 135)
    self.kanaAnswerText[i]:setYFlip(true)

    if i < iKanaMissed*2 + 1 then
      
      if i > iKanaMissed then
        Xaxis = position + 112 + 11 Yaxis = 237 - (i - iKanaMissed)*32
        self.kanaAnswerText[i]:setFont(romanjiFont1)
        self.kanaAnswerText[i]:setAlignment(MOAITextBox.LEFT_JUSTIFY, MOAITextBox.CENTER_JUSTIFY)
        self.kanaAnswerText[i]:setString(answerKana[i - iKanaMissed])
      else
        Xaxis = position - 112 - 11 Yaxis = 237 - i*32
        self.kanaAnswerText[i]:setFont(kanjiFont1)
        self.kanaAnswerText[i]:setAlignment(MOAITextBox.RIGHT_JUSTIFY, MOAITextBox.CENTER_JUSTIFY)
        self.kanaAnswerText[i]:setString(kanaMissed[i])
      end

      self.kanaAnswerText[i]:setLoc(Xaxis - 112, Yaxis - 38)

    elseif i < iKanaMissed*2 + 2 then
      Xaxis = 222 Yaxis = 230 - 66
      self.kanaAnswerText[i]:setFont(romanjiFont1)
      self.kanaAnswerText[i]:setLoc(Xaxis - 112, Yaxis - 38)
      self.kanaAnswerText[i]:setAlignment(MOAITextBox.CENTER_JUSTIFY, MOAITextBox.CENTER_JUSTIFY)
      if gameLevel % 3 ~= 0 then self.kanaAnswerText[i]:setString("Hits: " .. tostring(iHitsKana + iHitsBoss)) else self.kanaAnswerText[i]:setString("") end
    else
      Xaxis = position Yaxis = 237
      self.kanaAnswerText[i]:setFont(romanjiFont1)
      self.kanaAnswerText[i]:setLoc(Xaxis - 112, Yaxis - 38)
      self.kanaAnswerText[i]:setAlignment(MOAITextBox.CENTER_JUSTIFY, MOAITextBox.CENTER_JUSTIFY)
      self.kanaAnswerText[i]:setString("Kanas Lost: " .. tostring(iKanaMissed))
    end
  end
end

function Game:hideKanaAnswer()

  for i = 1, iKanaMissed*2 + 2 do hudLayer:removeProp(self.kanaAnswerText[i]) end

end

function Game:loadBossAnswer(position)
  self.bossAnswerText = {}

  local Xaxis, Yaxis = 0, 0

  for i = 1, iBossMissed*2 + 2 do

    self.bossAnswerText[i] = MOAITextBox.new()
    self.bossAnswerText[i]:setRect(0, 0, 224, 66)

    hudLayer:insertProp(self.bossAnswerText[i])

    self.bossAnswerText[i]:setColor(132, 134, 135)
    self.bossAnswerText[i]:setYFlip(true)

    if i < iBossMissed*2 + 1 then
      
      if i > iBossMissed then
        Xaxis = position + 112 + 11 Yaxis = 237 - (i - iBossMissed)*32
        self.bossAnswerText[i]:setFont(romanjiFont1)
        self.bossAnswerText[i]:setAlignment(MOAITextBox.LEFT_JUSTIFY, MOAITextBox.CENTER_JUSTIFY)
        self.bossAnswerText[i]:setString(answerBoss[i - iBossMissed])
      else
        Xaxis = position - 112 - 11 Yaxis = 237 - i*32
        self.bossAnswerText[i]:setFont(kanjiFont1)
        self.bossAnswerText[i]:setAlignment(MOAITextBox.RIGHT_JUSTIFY, MOAITextBox.CENTER_JUSTIFY)
        self.bossAnswerText[i]:setString(bossMissed[i])
      end

      self.bossAnswerText[i]:setLoc(Xaxis - 112, Yaxis - 38)

    elseif i < iBossMissed*2 + 2 then
      Xaxis = 222 Yaxis = 230 - 66
      self.bossAnswerText[i]:setFont(romanjiFont1)
      self.bossAnswerText[i]:setLoc(Xaxis - 112, Yaxis - 38)
      self.bossAnswerText[i]:setAlignment(MOAITextBox.CENTER_JUSTIFY, MOAITextBox.CENTER_JUSTIFY)
      if gameLevel % 3 == 0 then self.bossAnswerText[i]:setString("Hits: " .. tostring(iHitsKana + iHitsBoss)) else self.bossAnswerText[i]:setString("") end
    else
      Xaxis = position Yaxis = 237
      self.bossAnswerText[i]:setFont(romanjiFont1)
      self.bossAnswerText[i]:setLoc(Xaxis - 112, Yaxis - 38)
      self.bossAnswerText[i]:setAlignment(MOAITextBox.CENTER_JUSTIFY, MOAITextBox.CENTER_JUSTIFY)
      self.bossAnswerText[i]:setString("Bosses Lost: " .. tostring(iBossMissed))
    end
  end
end

function Game:hideBossAnswer()

  for i = 1, iBossMissed*2 + 2 do hudLayer:removeProp(self.bossAnswerText[i]) end

end

function Game:loadGameHelp()
end

function Game:hideGameHelp()
end

function Game:loadKanaHelp(kanaIndex)
  self.kanaHelpText = {}
  self.kanaHelpShip = {}

  local Xaxis, Yaxis = 0, 0
  
  for i = 1, #kanaLevel[kanaIndex]*2 + 1 do

    self.kanaHelpText[i] = MOAITextBox.new()
    self.kanaHelpText[i]:setRect(0, 0, 224, 66)
    Aux.centralize(self.kanaHelpText[i])

    hudLayer:insertProp(self.kanaHelpText[i])

    self.kanaHelpText[i]:setColor(132, 134, 135)
    self.kanaHelpText[i]:setYFlip(true)

    if i < #kanaLevel[kanaIndex]*2 + 1 then

      if i > #kanaLevel[kanaIndex] then
        Xaxis = -445 + (-#kanaLevel[kanaIndex] + i)*666/(#kanaLevel[kanaIndex] + 1) Yaxis = -27
        self.kanaHelpText[i]:setFont(romanjiFont1)
        self.kanaHelpText[i]:setString(Japones:kanaToText(kanaLevel[kanaIndex][-#kanaLevel[kanaIndex] + i]))
      else
        Xaxis = -445 + i*666/(#kanaLevel[kanaIndex] + 1) Yaxis = 55
        self.kanaHelpText[i]:setFont(kanjiFont1)
        self.kanaHelpText[i]:setString(kanaLevel[kanaIndex][i])

        self.kanaHelpShip[i] = MOAIProp2D.new()
        self.kanaHelpShip[i]:setDeck(ResourceManager:get("ship"))
        self.kanaHelpShip[i]:setLoc(Xaxis + 112, Yaxis + 33)
        self.kanaHelpShip[i]:setRot(135)
        self.kanaHelpShip[i]:setScl(0.7, 0.7)
        midLayer:insertProp(self.kanaHelpShip[i])
      end

      self.kanaHelpText[i]:setLoc(Xaxis, Yaxis)
    
    else
      Xaxis = -112 Yaxis = 187
      self.kanaHelpText[i]:setFont(romanjiFont1)
      self.kanaHelpText[i]:setLoc(Xaxis, Yaxis)
      self.kanaHelpText[i]:setString("Level " .. tostring(helpLevel[1][kanaIndex] .. "\n" .. Japones:kanaToText(kanaLevel[kanaIndex][1]) .. " Gyo"))
    end
  end
end

function Game:hideKanaHelp(kanaIndex)

  for i = 1, #kanaLevel[kanaIndex]*2 + 1 do hudLayer:removeProp(self.kanaHelpText[i]) if i <= #kanaLevel[kanaIndex] then midLayer:removeProp(self.kanaHelpShip[i]) end end

end

function Game:loadBossHelp(bossIndex, windowIndex, number)
  self.bossHelpText = {}
  self.bossHelpShip = {}

  local Xaxis, Yaxis, increment, windows = 0, 0, 0, 0
  
  increment = 4*(windowIndex - 1) if #bossLevel[bossIndex] < 5 then number = #bossLevel[bossIndex] end if #bossLevel[bossIndex] % 4 == 0 then windows = #bossLevel[bossIndex]/4 else windows = math.modf(#bossLevel[bossIndex]/4) + 1 end
  
  for i = 1, number*2 + 1 do

    self.bossHelpText[i] = MOAITextBox.new()
    self.bossHelpText[i]:setRect(0, 0, 224, 66)
    Aux.centralize(self.bossHelpText[i])

    hudLayer:insertProp(self.bossHelpText[i])

    self.bossHelpText[i]:setColor(132, 134, 135)
    self.bossHelpText[i]:setYFlip(true)

    self.bossHelpShip[i] = MOAIProp2D.new()

    midLayer:insertProp(self.bossHelpShip[i])

    if i < number*2 + 1 then

      if i > number then
        Xaxis = -445 + (-number + i)*666/(number + 1) Yaxis = -27
        self.bossHelpText[i]:setFont(romanjiFont1)
        self.bossHelpText[i]:setLoc(Xaxis, Yaxis)
        
        if dMode == "easy" then self.bossHelpText[i]:setString(Japones:kanaToText(bossLevel[bossIndex][-number + increment + i])) else self.bossHelpText[i]:setString("") end

        self.bossHelpShip[i]:setDeck(ResourceManager:get(Japones:kanaToText(bossLevel[bossIndex][-number + increment + i])))
        self.bossHelpShip[i]:setLoc(Xaxis + 112, Yaxis + 33 + 60)
        self.bossHelpShip[i]:setRot(0)
        self.bossHelpShip[i]:setScl(0.7, 0.7)
      else
        Xaxis = -445 + i*666/(number + 1) Yaxis = 32
        self.bossHelpText[i]:setFont(kanjiFont1)
        self.bossHelpText[i]:setLoc(Xaxis, Yaxis - 60)
        
        if dMode ~= "easy" then self.bossHelpText[i]:setString(bossLevel[bossIndex][increment + i]) else self.bossHelpText[i]:setString("") end

        self.bossHelpShip[i]:setDeck(ResourceManager:get("paraquedas"))
        self.bossHelpShip[i]:setLoc(Xaxis + 112, Yaxis + 33)
        self.bossHelpShip[i]:setRot(180)
        self.bossHelpShip[i]:setScl(0.7, 0.7)
      end
    
    else
      Xaxis = -112 Yaxis = 187
      self.bossHelpText[i]:setFont(romanjiFont1)
      self.bossHelpText[i]:setLoc(Xaxis, Yaxis)
      self.bossHelpText[i]:setString("Level " .. tostring(helpLevel[2][bossIndex]) .. "\n" .. "Page " .. tostring(windowIndex) .. "/" .. tostring(windows))
    end
  end
end

function Game:hideBossHelp(bossIndex, number)
  
  if #bossLevel[bossIndex] < 5 then number = #bossLevel[bossIndex] end

  for i = 1, number*2 + 1 do hudLayer:removeProp(self.bossHelpText[i]) midLayer:removeProp(self.bossHelpShip[i]) end

end

return Game