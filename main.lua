require "./code/config"     -- Read Config 
require "./code/globalVars" -- Read Global variables
local Menu = require "./code/menu" 
local Game = require "./code/game"
-- Open main screen
MOAISim.openWindow("Karucha Ships Invaders", ScreenX, ScreenY)
-- Setup viewport
viewport = MOAIViewport.new()
viewport:setSize(ScreenX, ScreenY)
viewport:setScale(WorldX, WorldY)

function mainLoop()
    MOAIUntzSystem.initialize() -- Start sound system 
    while true do
        Menu:start()
        Game:start()
    end
end

gameThread = MOAICoroutine.new()
gameThread:run(mainLoop)

-- for index = 1, 4 do
-- 	if index < 3 then
-- 		dateHour = dateHour:sub(1, dateHour:find('/') - 1) .. "-" .. dateHour:sub(dateHour:find('/') + 1)
-- 	else
-- 		dateHour = dateHour:sub(1, dateHour:find(':') - 1) .. "." .. dateHour:sub(dateHour:find(':') + 1)
-- 	end
-- end

-- file = assert(io.open("./Logs/User Log " .. dateHour .. ".txt", "w"), "Error.")
-- file:write("____________________________\n\n User Log " .. os.date() .. "\n\n" .. " Information of the match:\n____________________________\n")
-- file:flush()
-- io.close(file)